﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MusicPlaylist : MonoBehaviour
{
    // Start is called before the first frame update
    public AudioClip[] m_playlist;
    private AudioSource m_source;
    private int m_currentTrack = 0;
    private float m_Volume = 0f;

    void Start()
    {
        m_source = GetComponent<AudioSource>();
        m_currentTrack = Mathf.FloorToInt(Random.value * m_playlist.Length);
        m_source.clip = m_playlist[m_currentTrack];
        m_source.Play();
        m_Volume = m_source.volume;
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.M))
        {
            if (!Mathf.Approximately(m_source.volume, m_Volume))
                m_source.volume = m_Volume;
            else
                m_source.volume = 0f;
        }
        if( m_source.isPlaying == false )
        {
            m_currentTrack = (m_currentTrack + 1) % m_playlist.Length;
            m_source.clip = m_playlist[m_currentTrack];
            m_source.Play();
        }
    }
}
