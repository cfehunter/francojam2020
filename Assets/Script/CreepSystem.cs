﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using Random = UnityEngine.Random;

public class CreepSystem : MonoBehaviour
{
    [Header("Times")]
    public float WireDamageDelay = 1.0f;
    private float m_CurrentDamageTimer = 0f;

    [Header("Steering Forces")]
    public float CohesionStrength = 1.0f;
    public float SeparationStrength = 1.0f;
    public float AlignmentStrength = 0.5f;
    public float PathFollowForce = 1.5f;

    [Header("Distances")]
    public float           SeparationDistance = 0.2f;
    public float           CohesionDistance = 0.5f;
    public float           FlockDistance = 1.5f;
    public float           PathFollowCutoff = 0.5f;
    public LayerMask       CreepOverlapFilter = Physics2D.AllLayers;
    public float SqrFlockDistance => FlockDistance * FlockDistance;

    public int NumberOfCreepsInWave { get; private set; } = 1;

#if UNITY_EDITOR
    public bool DrawForceLines = false;
#endif

    void FixedUpdate()
    {
        if (Creep.ActiveCreeps.Count > 0)
        {
            UpdatePaths();
            UpdateCreepNeighbours();
            CalculateSteeringForces();
            TickWalls();
        }
    }

    void OnDestroy()
    {
        if (GameSession.Instance && GameSession.Instance.CreepSystem == this)
        {
            GameSession.Instance.CreepSystem = null;
        }
    }

    private void UpdateCreepNeighbours()
    {
        float overlap_radius = FlockDistance;
        Collider2D[] overlap_results = new Collider2D[Creep.MaxNeighbours + 1];

        foreach (Creep current_creep in Creep.ActiveCreeps)
        {
            overlap_results.Fill(null);
            int overlapped_count = Physics2D.OverlapCircleNonAlloc(current_creep.Rigidbody.position, overlap_radius, overlap_results, CreepOverlapFilter);

            List<Creep> neighbours = current_creep.SteeringNeighbours;
            neighbours.Clear();
            for (int overlap_index = 0; overlap_index < overlapped_count; ++overlap_index)
            {
                Creep neighbour = overlap_results[overlap_index].gameObject.GetComponent<Creep>();
                if (neighbour != null && neighbour != current_creep)
                {
                    neighbours.Add(neighbour);
                }
            }
        }
    }

    private void UpdatePaths()
    {
        float sqr_cutoff = PathFollowCutoff;
        foreach (Creep c in Creep.ActiveCreeps)
        {
            if (c.Path.Count > 1)
            {
                if ((c.Rigidbody.position - c.Path[0]).sqrMagnitude < sqr_cutoff)
                {
                    c.Path.RemoveAt(0);
                }
            }
        }
    }

    public bool SpawnCreepsForWave(int wave)
    {
        int amount_spawned = 0;
        foreach (CreepSpawner spawner in CreepSpawner.ActiveSpawners)
        {
            amount_spawned += spawner.SpawnWave(wave);
        }
        NumberOfCreepsInWave = amount_spawned;
        return amount_spawned > 0;
    }

    private void TickWalls()
    {
        m_CurrentDamageTimer -= Time.deltaTime;
        if (m_CurrentDamageTimer <= 0f)
        {
            m_CurrentDamageTimer = WireDamageDelay;
            DamageWalls();
            DamageCore();
        }
    }

    private void DamageCore()
    {
        DefenseCore core = DefenseCore.ActiveDefenseCore;
        if (!core)
            return;

        foreach (Creep c in Creep.ActiveCreeps)
        {
            if (c.Rigidbody.IsTouching(core.CoreCollider))
            {
                core.Damage(c.Definition.CoreDamage);
            }
        }
    }

    private void DamageWalls()
    {
        IReadOnlyList<TurretWall> walls = TurretWall.ActiveWalls;
        for (int i = walls.Count - 1; i >= 0; --i)
        {
            TurretWall wall = walls[i];

            //Only tick walls that can be destroyed
            if (wall.Health > 0 && wall.IsPowered)
            {
                foreach (Creep c in Creep.ActiveCreeps)
                {
                    if (c.Rigidbody.IsTouching(wall.Collider))
                    {
                        //Damage the wall. Break if destroyed
                        if (wall.Damage(c.Definition.WireDamage))
                            break;
                    }
                }
            }
        }
    }

    #region Steering Behaviours

    private void CalculateSteeringForces()
    {
        foreach (Creep c in Creep.ActiveCreeps)
        {
            Vector2 cohesion_force   = CalculateCoheisionForce(c) * CohesionStrength;
            Vector2 separation_force = CalculateSeparationForce(c, SeparationDistance) * SeparationStrength;
            Vector2 alignment_force  = CalculateAlignmentForce(c) * AlignmentStrength;
            Vector2 path_force       = CalculatePathForce(c) * PathFollowForce;

            c.SteerForces = cohesion_force + separation_force + alignment_force + path_force;
        }
    }

    private static Vector2 CalculateSeek(Creep c, Vector2 target)
    {
        Vector2 desired = target - c.Rigidbody.position;
        float seek_distance = desired.magnitude;
        if (seek_distance > 0f)
        {
            desired *= (c.Definition.MaxSpeed / desired.magnitude);

            Vector2 force = desired - c.Rigidbody.velocity;
            return force * (c.Definition.MaxAcceleration / c.Definition.MaxSpeed);
        }
        return Vector2.zero;
    }

    private static Vector2 CalculateCoheisionForce(Creep c)
    {
        if (c.SteeringNeighbours.Count > 0)
        {
            Vector2 center_of_mass = Vector2.zero;
            foreach (Creep neighbour in c.SteeringNeighbours)
            {
                Vector2 neighbour_position = neighbour.Rigidbody.position;
                center_of_mass += neighbour_position;
            }
            center_of_mass /= c.SteeringNeighbours.Count;

            return CalculateSeek(c, center_of_mass);
        }
        return Vector2.zero;
    }

    private static Vector2 CalculateSeparationForce(Creep c, float separation_distance)
    {
        Vector2 separation_force = Vector2.zero;
        if (c.SteeringNeighbours.Count > 0)
        {

            Vector2 position = c.Rigidbody.position;
            int separation_count = 0;
            foreach (Creep neighbour in c.SteeringNeighbours)
            {
                Vector2 neighbour_position = neighbour.Rigidbody.position;
                Vector2 from = position - neighbour_position;
                float distance = from.magnitude;
                if (distance < separation_distance)
                {
                    ++separation_count;
                    if (distance > 0f)
                    {
                        separation_force += from / separation_distance;
                    }
                }
            }

            if (separation_count > 0)
                separation_force = (separation_force / separation_count);
        }
        return separation_force;
    }

    private static Vector2 CalculateAlignmentForce(Creep c)
    {
        if (c.SteeringNeighbours.Count > 0)
        {
            Vector2 average_heading = Vector2.zero;
            foreach (Creep neighbour in c.SteeringNeighbours)
            {
                Vector2 neighbour_position = neighbour.Rigidbody.position;
                average_heading += neighbour.Rigidbody.velocity.normalized;
            }
            average_heading /= c.SteeringNeighbours.Count;

            Vector2 desired = average_heading * c.Definition.MaxSpeed;
            Vector2 adjusted = desired - c.Rigidbody.velocity;
            return adjusted * (c.Definition.MaxAcceleration / c.Definition.MaxSpeed);
        }
        return Vector2.zero;
    }

    private static Vector2 CalculatePathForce(Creep c)
    {
        if (c.Path.Count == 0)
            return Vector2.zero;

        return CalculateSeek(c, c.Path[0]);
    }

    #endregion
}
