﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

#if UNITY_EDITOR
public class IntersectionTest : MonoBehaviour
{
    public List<Vector2> PolyPoints = new List<Vector2>();
    public Vector2 Start = Vector2.zero;
    public Vector2 End = Vector2.zero;
    public Vector2 Point = Vector2.zero;
    public Vector2 Start2 = Vector2.zero;
    public Vector2 End2 = Vector2.zero;
    public Vector2 EllipseSize = Vector2.one;
    public int EllpiseSegments = 20;
    public Vector2 EllipsePoint = Vector2.zero;

    private void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.blue;
        Vector3 position = transform.position;
        for (int i = 0, end = PolyPoints.Count; i < end; ++i)
        {
            int next_point = (i + 1) % end;
            Gizmos.DrawLine(position + PolyPoints[i].ToVector3(), position + PolyPoints[next_point].ToVector3());
        }

        Vector2 intersection = Vector2.zero;
        bool line_intersected = SpatialUtil.PolygonIntersection(PolyPoints, Start, End, out intersection);

        if (line_intersected)
        {
            Gizmos.color = Color.green;
            Gizmos.DrawLine(position + Start.ToVector3(), position + End.ToVector3());
        }
        else
        {
            Gizmos.color = Color.red;
            Gizmos.DrawLine(position + Start.ToVector3(), position + End.ToVector3());
        }

        if (SpatialUtil.LineIntersection(Start, End, Start2, End2))
        {
            Gizmos.color = Color.cyan;
            Gizmos.DrawLine(position + Start2.ToVector3(), position + End2.ToVector3());
        }
        else
        {
            Gizmos.color = Color.magenta;
            Gizmos.DrawLine(position + Start2.ToVector3(), position + End2.ToVector3());
        }

        bool point_inside = SpatialUtil.IsPointInsidePolygon(PolyPoints, Point);
        Gizmos.color = point_inside ? Color.green : Color.red;
        Gizmos.DrawSphere(transform.position + Point.ToVector3(), 0.2f);

        Gizmos.color = Color.magenta;
        FrancoJamGizmoUtil.DrawEllipse(transform.position, EllipseSize.x, EllipseSize.y, EllpiseSegments);

        bool point_in_ellipse = SpatialUtil.IsPointInsideEllipse(Vector2.zero, EllipseSize.x, EllipseSize.y, EllipsePoint); ;
        Gizmos.color = point_in_ellipse ? Color.green : Color.red;
        Gizmos.DrawCube(transform.position + EllipsePoint.ToVector3(), new Vector3(0.2f, 0.2f, 0.2f));
    }
}
#endif //UNITY_EDITOR
