﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(CircleCollider2D))]
public class DefenseCore : MonoBehaviour
{
    public static DefenseCore ActiveDefenseCore { get; private set; }
    public static event Action<DefenseCore> DefenseCoreDamaged;
    public static event Action<DefenseCore> DefenseCoreLowHealth;


    [SerializeField] private int m_Health = 10;
    public int                   MaxHealth { get; private set; }
    public int                   LowHealthThreshold = 3;
    public int                   Health => m_Health;
    public CircleCollider2D      CoreCollider { get; private set; }

    private void Awake()
    {
        ActiveDefenseCore = this;
        CoreCollider = GetComponent<CircleCollider2D>();

        MaxHealth = m_Health;
    }

    private void OnDestroy()
    {
        if (ActiveDefenseCore == this)
            ActiveDefenseCore = null;
    }

    public void Damage(int amount)
    {
        if (amount > 0 && GameSession.Instance.State == GameState.Playing)
        {
            int prev_health = m_Health;
            m_Health -= amount;
            m_Health = Mathf.Max(m_Health, 0);
            DefenseCoreDamaged?.Invoke(this);

            if (prev_health > LowHealthThreshold && m_Health <= LowHealthThreshold)
            {
                DefenseCoreLowHealth?.Invoke(this);
            }
        }
    }
}
