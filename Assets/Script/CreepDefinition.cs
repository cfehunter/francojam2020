﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName ="Franco Jam/Creep Definition")]
public class CreepDefinition : ScriptableObject
{
    public float MaxSpeed = 0.5f;
    public float MaxAcceleration = 0.75f;
    public int   HitPoints = 1;
    public int   CoreDamage = 1;
    public int   WireDamage = 1;
    public int   SpawnCost = 1;
    public float SqrMaxSpeed => MaxSpeed * MaxSpeed;
    public float SqrMaxAcceleration => MaxAcceleration * MaxAcceleration;
    public ExplosionEffect ExplosionEffect;

    private void OnEnable()
    {
        SpawnCost = Mathf.Max(1, SpawnCost);
    }
}
