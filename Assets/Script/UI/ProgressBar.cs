﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

enum ProgressFormatMode
{
    Percentage,
    XofY
}

[RequireComponent(typeof(Image))]
public class ProgressBar : MonoBehaviour
{
    private float m_Value = 0f;
    [SerializeField] private float m_Max = 100f;
    [SerializeField] private float m_Min = 0f;
    [SerializeField] private ProgressFormatMode m_TextMode = ProgressFormatMode.Percentage;

    public float NormalisedValue
    {
        get => m_Value / m_Max;
        set
        {
            float clamped = Mathf.Clamp01(value);
            float new_value = m_Min + (m_Max - m_Min) * clamped;
            if (!Mathf.Approximately(m_Value, new_value))
            {
                m_Value = new_value;
                enabled = true;
            }
        }
    }

    public float Min
    {
        get => m_Min;
        set
        {
            m_Min = value;
            m_Max = Mathf.Max(m_Min, m_Max);
            enabled = true;
        }
    }

    public float Max
    {
        get => m_Max;
        set
        {
            m_Max = value;
            m_Min = Mathf.Min(m_Min, m_Max);
            enabled = true;
        }
    }

    public float Value
    {
        get => m_Value;
        set
        {
            m_Value = Mathf.Clamp(value, m_Min, m_Max);
            enabled = true;
        }
    }

    private Text  m_Label;
    private Image m_Bar;

    private void Awake()
    {
        m_Label = transform.Find("Label")?.GetComponent<Text>();
        m_Bar = transform.Find("Bar")?.GetComponent<Image>();
        Debug.Assert(m_Bar);
    }

    private void Update()
    {
        UpdateLayout();
    }

    private void UpdateLayout()
    {
        if (m_Label)
        {
            float normalised_value = NormalisedValue;
            m_Bar.fillAmount = normalised_value;

            switch (m_TextMode)
            {
                case ProgressFormatMode.Percentage:
                    m_Label.text = $"{Mathf.FloorToInt(normalised_value * 100f)}%"; 
                    break;
                case ProgressFormatMode.XofY:
                    m_Label.text = $"{m_Value}/{m_Max}";
                    break;
            }
            enabled = false;
        }
    }
}
