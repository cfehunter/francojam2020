﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameHUD : MonoBehaviour
{
    private ProgressBar m_WaveProgress;
    private ProgressBar m_PowerBar;
    private ProgressBar m_CoreHealthBar;
    private Text        m_WaveIndexText;
    private Text        m_SplashText;
    private DialogBox   m_DialogBox;

    private static GameHUD Instance { get; set; }

    [SerializeField]
    private float       m_SplashTime = 2f;

    private void Awake()
    {
        Instance = this;

        m_WaveProgress = transform.Find("WaveStats/WaveProgressBar")?.GetComponent<ProgressBar>();
        Debug.Assert(m_WaveProgress);
        m_WaveProgress.NormalisedValue = 0f;

        m_WaveIndexText = transform.Find("WaveStats/LevelProgressHeader")?.GetComponent<Text>();
        Debug.Assert(m_WaveProgress);

        m_SplashText = transform.Find("SplashText")?.GetComponent<Text>();
        Debug.Assert(m_SplashText);
        m_SplashText.enabled = false;

        m_PowerBar = transform.Find("WaveStats/Power")?.GetComponent<ProgressBar>();

        m_CoreHealthBar = transform.Find("WaveStats/Core")?.GetComponent<ProgressBar>();
        Debug.Assert(m_CoreHealthBar);

        m_DialogBox = GetComponentInChildren<DialogBox>(true);
        Debug.Assert(m_DialogBox);

        Creep.CreepDestroyed += OnCreepDestroyed;
        GameSession.WaveStarted += OnWaveStarted;
        TurretBehaviour.UsedGridPowerChanged += OnUsedPowerChanged;
        DefenseCore.DefenseCoreDamaged += OnCoreDamaged;
        CutscenePlayer.OnCutsceneDialogChanged += OnCutsceneDialogChanged;
    }

    private void Start()
    {
        UpdateWaveIndex();
        UpdatePower();
        UpdateCore();
    }

    private void OnDestroy()
    {
        Creep.CreepDestroyed -= OnCreepDestroyed;
        GameSession.WaveStarted -= OnWaveStarted;
        TurretBehaviour.UsedGridPowerChanged -= OnUsedPowerChanged;
        DefenseCore.DefenseCoreDamaged -= OnCoreDamaged;
        CutscenePlayer.OnCutsceneDialogChanged -= OnCutsceneDialogChanged;

        if (Instance == this)
            Instance = null;
    }

    private void UpdateWaveIndex()
    {
        GameSession active_game = GameSession.Instance;
        if (active_game)
        {
            int wave_count = active_game.Waves.Count;
            int last_wave_index = wave_count - 1;
            if (active_game.CurrentWave < last_wave_index)
            {
                m_WaveIndexText.text = string.Format($"Wave {active_game.CurrentWave + 1}/{wave_count}");
            }
            else
            {
                m_WaveIndexText.text = "FINAL WAVE";
            }
        }
    }

    private void UpdatePower()
    {
        m_PowerBar.Max = GameSession.Instance.PowerLimit;
        m_PowerBar.Value = GameSession.Instance.PowerLimit - TurretBehaviour.UsedGridPower;

    }

    private void UpdateCore()
    {
        m_CoreHealthBar.Max = DefenseCore.ActiveDefenseCore.MaxHealth;
        m_CoreHealthBar.Value = DefenseCore.ActiveDefenseCore.Health;
    }

    private void OnCoreDamaged(DefenseCore obj)
    {
        UpdateCore();
    }

    private void OnUsedPowerChanged(int new_power)
    {
        UpdatePower();
    }

    private void OnCreepDestroyed(Creep obj)
    {
        GameSession active_game = GameSession.Instance;
        if (active_game)
        {
            int total_alive = CreepSpawner.TotalSpawnsPending + Creep.ActiveCreeps.Count;
            int to_spawn = active_game.CreepSystem.NumberOfCreepsInWave;
            m_WaveProgress.NormalisedValue = 1f - total_alive / (float)to_spawn;
        }
    }

    private void OnWaveStarted(GameWaveDefinition wave, int wave_number)
    {
        m_WaveProgress.NormalisedValue = 0f;
        UpdateWaveIndex();
        UpdatePower();


        if (wave.SplashText.Length > 0)
        {
            m_SplashText.text = wave.SplashText;
            m_SplashText.enabled = true;
            GameSession.Instance.DeferredCallbacks.AddCallback(() => m_SplashText.enabled = false, m_SplashTime);
        }
    }

    private void OnCutsceneDialogChanged(DialogLine obj)
    {
        m_DialogBox.gameObject.SetActive(obj != null);
        if (obj != null)
        {
            m_DialogBox.ApplyDialogLine(obj);
        }
    }
}
