﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

[Serializable]
public class FrontendSlide
{
    public Transform Slide;
    public float     Time;
}

public class Frontend : MonoBehaviour
{
    public List<FrontendSlide> Slides = new List<FrontendSlide>();
    public int m_NextScene = 0;
    int m_SlideIndex = 0;
    float m_SlideTimer = 0f;

    private void Awake()
    {
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetButtonDown("Submit"))
        {
            AdvanceSlide();
        }
        else if (Slides[m_SlideIndex].Time > 0f)
        {
            m_SlideTimer += Time.deltaTime;
            if (m_SlideTimer >= Slides[m_SlideIndex].Time)
            {
                AdvanceSlide();
            }
        }
    }

    private void AdvanceSlide()
    {
        m_SlideTimer = 0f;
        ++m_SlideIndex;
        if (m_SlideIndex < Slides.Count)
        {
            for (int i = 0; i < Slides.Count; ++i)
            {
                Slides[i].Slide.gameObject.SetActive(i == m_SlideIndex);
            }
        }
        else
        {
            SceneManager.LoadScene(m_NextScene);
        }
    }
}
