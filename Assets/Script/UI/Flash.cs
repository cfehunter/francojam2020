﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Flash : MonoBehaviour
{
    [SerializeField] private float m_FlashDelay;
    private float m_CurrentFlashTime = 0f;
    bool          m_Visible = false;

    Graphic[]     m_Graphics;

    private void Awake()
    {
        m_Graphics = GetComponentsInChildren<Graphic>();
    }

    private void Update()
    {
        m_CurrentFlashTime += Time.deltaTime;
        if (m_CurrentFlashTime >= m_FlashDelay)
        {
            m_CurrentFlashTime = 0f;
            m_Visible = !m_Visible;

            foreach (Graphic g in m_Graphics)
            {
                g.enabled = m_Visible;
            }
        }
    }

}
