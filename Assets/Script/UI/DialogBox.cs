﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(AudioSource))]
public class DialogBox : MonoBehaviour
{
    private Transform   m_NamePlate;
    private Text        m_NameText;
    private Image       m_Portrait;
    private Text        m_Content;
    private AudioSource m_AudioSource;

    public string     Name             { get => m_NameText.text; set { m_NameText.text = value; ShowNamePlate = value.Length > 0; } }
    public Color      NameColor        { get => m_NameText.color; set => m_NameText.color = value; }
    public string     Content          { get => m_Content.text; set => m_Content.text = value; }
    public TextAnchor ContentAlignment { get => m_Content.alignment; set => m_Content.alignment = value; }
    public Color      ContentColour    { get => m_Content.color; set => m_Content.color = value; }
    public Sprite     Portrait         { get => m_Portrait.sprite; set { m_Portrait.sprite = value; ShowPortrait = value; } }
    public bool       ShowNamePlate    { get => m_NamePlate.gameObject.activeSelf; set => m_NamePlate.gameObject.SetActive(value); }
    public bool       ShowPortrait
    {
        get => m_Portrait.gameObject.activeSelf;
        set
        {
            if (value != ShowPortrait)
            {
                RectTransform content_transform = m_Content.rectTransform;
                float portrait_offset = (m_Portrait.rectTransform.rect.width + m_Portrait.rectTransform.rect.xMin);
                Vector3 movement = portrait_offset * (value ? Vector3.right : -Vector3.right);
                content_transform.position += movement;
                content_transform.sizeDelta -= movement.XY();
                m_Portrait.gameObject.SetActive(value);
            }
        }
    }

    public void ApplyDialogLine(DialogLine line)
    {
        Name = line.NameText;
        NameColor = line.NameColour;
        Content = line.ContentText;
        ContentAlignment = line.ContentAlignment;
        ContentColour = line.ContentColour;
        Portrait = line.Portrait;
        PlayAudio(line.Audio);
    }

    private void PlayAudio(AudioClip audio)
    {
        m_AudioSource.Stop();
        m_AudioSource.clip = audio;

        if (audio)
        {
            m_AudioSource.Play();
        }
    }

    private void Awake()
    {
        m_NamePlate = transform.Find("NamePlate");
        Debug.Assert(m_NamePlate);

        m_NameText = m_NamePlate.GetComponentInChildren<Text>();
        Debug.Assert(m_NameText);

        m_Portrait = transform.Find("Portrait")?.GetComponent<Image>();
        Debug.Assert(m_Portrait);

        m_Content = transform.Find("Content")?.GetComponent<Text>();
        Debug.Assert(m_Content);

        m_AudioSource = this.RequireComponent<AudioSource>();
    }
}
