﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    [SerializeField]private TurretWire m_WirePrefab;
    public float m_movementSpeed = 1.0f;
    public float m_turretInteractionRange = 1.0f;
    public float m_maxWireRange = 5.0f;
    private bool m_isWireCloseToMaxRange = false;

    private SpriteRenderer m_spriteRenderer;
    private Animator m_animator;
    private Rigidbody2D m_rigidBody;

    private Vector2 m_inputVector;
    private TurretBehaviour m_nearestTurret;
    private TurretBehaviour m_turretDraggingWireFrom;
    private bool m_interactPressed = false;
    private bool m_interactHeld = false;
    private bool m_togglePressed = false;
    private bool m_toggleHeld = false;

    private bool m_breakPressed = false;
    private bool m_breakHeld = false;

    private TurretWire m_WireInstance;

    private void Awake()
    {
        m_spriteRenderer = GetComponent<SpriteRenderer>();
        m_animator = GetComponent<Animator>();
        m_rigidBody = GetComponent<Rigidbody2D>();

        if (m_WirePrefab)
        {
            m_WireInstance = Instantiate(m_WirePrefab);
            m_WireInstance.EndObject = transform;
            m_WireInstance.gameObject.SetActive(false);
        }
    }

    // Update is called once per frame
    void PollButtonInput()
    {
        {
            var wasHeld = m_interactHeld;
            m_interactHeld = Input.GetButton("Interact");
            if (m_interactHeld && wasHeld != m_interactHeld)
            {
                m_interactPressed = true;
            }
            else if (m_interactPressed)
            {
                m_interactPressed = false;
            }
        }

        {
            var wasHeld = m_toggleHeld;
            m_toggleHeld = Input.GetButton("ToggleTurret");
            if (m_toggleHeld && wasHeld != m_toggleHeld)
            {
                m_togglePressed = true;
            }
            else if (m_togglePressed)
            {
                m_togglePressed = false;
            }
        }

        {
            var wasHeld = m_breakHeld;
            m_breakHeld = Input.GetButton("BreakWires");
            if (m_breakHeld && wasHeld != m_breakHeld)
            {
                m_breakPressed = true;
            }
            else if (m_breakPressed)
            {
                m_breakPressed = false;
            }
        }
    }
    void FixedUpdate()
    {
        PollButtonInput();
        PlayerMovement();

        if (m_turretDraggingWireFrom)
        {
            // We are currently dragging a wire, make sure it doesn't intersect with any of the existing connections
            TestWireIntersections();
        }

        m_nearestTurret = TurretBehaviour.GetNearestTurret(transform.position2D(), m_turretInteractionRange * m_turretInteractionRange);

        if (m_interactPressed)
        {
            // User is trying to interact, handle the input
            TestInteractInput();
        }

        m_nearestTurret?.HighlightTurret();

        if(m_breakPressed)
        {
            DropWire();
            if (m_nearestTurret)
            {
                m_nearestTurret?.DisconnectAllWalls();
            }
        }

        if( m_togglePressed && m_nearestTurret )
        {
            m_nearestTurret.ToggleWeapon();
        }
    }
#if UNITY_EDITOR
    void OnDrawGizmos()
    {
        Gizmos.color = Color.blue;
        if (m_nearestTurret != null)
        {
            Gizmos.DrawWireSphere(m_nearestTurret.transform.position2D(), m_turretInteractionRange);
        }
        if (m_turretDraggingWireFrom != null)
        {
            if(m_isWireCloseToMaxRange)
            {
                Gizmos.color = Color.red;
            }
            Gizmos.DrawWireSphere(m_turretDraggingWireFrom.transform.position2D(), 0.2f);
            Gizmos.DrawLine(m_turretDraggingWireFrom.transform.position2D(), transform.position2D());
        }

        TurretBehaviour.DrawTurretConnections();
    }
#endif
    void PlayerMovement()
    {
        
        m_inputVector.x = Input.GetAxis("Horizontal");
        m_inputVector.y = Input.GetAxis("Vertical");
        var _magnitude = Mathf.Clamp(m_inputVector.magnitude * 1.5f, 0.0f, 1.0f);
        m_inputVector = m_inputVector.normalized * _magnitude;

        if (m_inputVector.magnitude > 0.1f)
        {
            var newPos = m_inputVector * m_movementSpeed * Time.fixedDeltaTime;
            newPos += transform.position2D();

            if (m_turretDraggingWireFrom != null)
            {
                var dragVector = m_turretDraggingWireFrom.transform.position2D() - newPos;
                m_isWireCloseToMaxRange = (dragVector.sqrMagnitude > ((m_maxWireRange * m_maxWireRange) * 0.95f));
                if (dragVector.sqrMagnitude > (m_maxWireRange * m_maxWireRange))
                {
                    var dir = dragVector.normalized;
                    dir *= m_maxWireRange;
                    newPos = m_turretDraggingWireFrom.transform.position2D() - dir;
                }
            }

            m_rigidBody.MovePosition(newPos);


            //transform.localPosition += new Vector3(m_inputVector.x * m_movementSpeed * Time.fixedDeltaTime, m_inputVector.y * m_movementSpeed * Time.fixedDeltaTime, 0);
            m_spriteRenderer.flipX = m_spriteRenderer.flipX ? !(m_inputVector.x >= 0.1f) : m_inputVector.x <= -0.1f;
            m_animator.Play("PlayerWalk");
            m_animator.SetFloat("anim_rate", m_inputVector.magnitude * (m_movementSpeed / 2.0f));

        }
        else
        {
            m_animator.Play("PlayerIdle");
            m_rigidBody.velocity = Vector2.zero;
        }
    }

    void TestInteractInput()
    {
        // Check if we were close enough to a turret
        if (m_nearestTurret != null)
        {
            if (m_turretDraggingWireFrom == null)
            {
                // Close enough to a turret, check if we can pick up a wire
                PickUpWire();
            }
            else
            {
                // Close enough to a turret, check if we can hook it up
                ConnectToTurret();
            }
        }
        else
        {
            // Nothing to interact with, drop the wire if we had one
            DropWire();
        }
    }

    void PickUpWire()
    {
        m_turretDraggingWireFrom = m_nearestTurret;
        m_WireInstance.Begin = m_turretDraggingWireFrom.transform.position2D();
        m_WireInstance.gameObject.SetActive(true);
    }

    void DropWire()
    {
        m_turretDraggingWireFrom = null;
        m_WireInstance?.gameObject.SetActive(false);
    }

    void ConnectToTurret()
    {
        TurretBehaviour.FormTurretConnection(m_turretDraggingWireFrom, m_nearestTurret);
        DropWire();
    }

    void TestWireIntersections()
    {
        Vector2 currentWireStart = m_turretDraggingWireFrom.transform.position2D();
        Vector2 currentWireEnd = transform.position2D();

        if(TurretBehaviour.ConnectionIntersectionTest(currentWireStart, currentWireEnd))
        {
            // Wire intersected with an existing one, so we drop it
            DropWire();
        }
    }
}
