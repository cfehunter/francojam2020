﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(SpriteRenderer), typeof(EdgeCollider2D))]
public class TurretWire : MonoBehaviour
{
    [SerializeField] private float m_CapLengthAdjustment = 0f;
    [SerializeField] private Transform m_Cap;
    private EdgeCollider2D m_Collider;
    private SpriteRenderer m_Sprite;
    private Vector2  m_LastEnd;
    public Transform EndObject;

    private Vector2  m_Begin;
    public Vector2 Begin
    {
        get => m_Begin;
        set
        {
            m_Begin = value;
            RepositionAndScale();
        }
    }

    private void Awake()
    {
        m_Collider = GetComponent<EdgeCollider2D>();
        m_Sprite = GetComponent<SpriteRenderer>();
        m_Begin = transform.position2D();
    }

    private void FixedUpdate()
    {
        if (EndObject)
        {
            Vector2 new_end = EndObject.position2D();
            if (!new_end.Approximately(m_LastEnd))
            {
                m_LastEnd = new_end;
                RepositionAndScale();
            }
        }
    }

    private void OnEnable()
    {
        RepositionAndScale();
    }

    private void RepositionAndScale()
    {
        if (EndObject)
        {
            ScaleToPoints(m_Begin, EndObject.position2D());
        }
    }

    private void ScaleToPoints(Vector2 begin, Vector2 end)
    {
        if (begin.Approximately(end))
            return;

        SpriteRenderer sprite = m_Sprite ? m_Sprite : GetComponent<SpriteRenderer>();
        EdgeCollider2D collider = m_Collider ? m_Collider : GetComponent<EdgeCollider2D>();

        Vector2 to_end = end - begin;
        float length = to_end.magnitude;
        Vector2 to_end_dir = to_end / length;

        //Apply offset to cut the end short
        end -= to_end_dir * m_CapLengthAdjustment;
        length -= m_CapLengthAdjustment;

        Vector3 position = transform.position;
        Vector2 position_2d = (begin + end) * 0.5f;
        transform.position = new Vector3(position_2d.x, position_2d.y, position.z);
        transform.rotation = Quaternion.FromToRotation(Vector2.right, to_end_dir);
        sprite.size = new Vector2(Mathf.Abs(length), sprite.size.y);

        float half_length = length * 0.5f;
        collider.points = new Vector2[2] { new Vector2(-half_length, 0f), new Vector2(half_length, 0f) };

        Vector3 cap_pos = m_Cap.position;
        m_Cap.position = new Vector3(end.x, end.y, cap_pos.z);
        m_Cap.rotation = Quaternion.FromToRotation(Vector3.up, -to_end_dir);
    }
}
