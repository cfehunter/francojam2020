﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

using Random = UnityEngine.Random;

[RequireComponent(typeof(CircleCollider2D), typeof(Rigidbody2D))]
public class Creep : MonoBehaviour, IPooledObject
{
    //Statics
    static List<Creep> s_ActiveCreeps = new List<Creep>();
    public static IReadOnlyList<Creep> ActiveCreeps => s_ActiveCreeps;

    //Events
    public static event Action<Creep> CreepDestroyed;

    //Constants
    public const int MaxNeighbours = 5;

    [SerializeField] private CreepDefinition m_Definition;
    public CreepDefinition Definition { get => m_Definition; }

    //Runtime
    public Rigidbody2D      Rigidbody { get; private set; }
    public Vector2          SteerForces { get; set; } = Vector2.zero;
    public float FrictionRatio { get; set; } = 0.95f;
    public List<Creep>      SteeringNeighbours { get; set; } = new List<Creep>(MaxNeighbours);
    public Vector2          Velocity { get; private set; } = Vector2.zero;
    public bool             RequireJostle { get; set; } = false;
    public int              PrefabID { get; set; }
    List<Vector2>           m_Path = new List<Vector2>();
    public List<Vector2>    Path
    {
        get => m_Path;
        set { m_Path = new List<Vector2>(value); }
    }

    public int HitPoints { get; private set; }
    public int MaxHitPoints => m_Definition.HitPoints;

    SpriteRenderer m_spriteRenderer = null;

    private void Awake()
    {
        Rigidbody = GetComponent<Rigidbody2D>();
        Debug.Assert(Rigidbody);

        m_spriteRenderer = GetComponent<SpriteRenderer>();
        Debug.Assert(m_Definition != null, $"Creep [{name}] does not have settings");
        if (m_Definition == null)
        {
            return;
        }

        HitPoints = Definition.HitPoints;
    }

    private void OnEnable()
    {
        s_ActiveCreeps.Add(this);
        HitPoints = MaxHitPoints;
    }

    private void OnDisable() //This is also called on destruction
    {
        s_ActiveCreeps.QuickRemove(this);
    }

    private void FixedUpdate()
    {
        Vector2 position = Rigidbody.position;
        Vector2 movement_vector = Vector2.zero;

        //Force a jostle if two creeps are exactly ontop of eachother
        if (SteeringNeighbours.Any(c => MathUtil.Approximately(c.Rigidbody.position, position)))
        {
            movement_vector += (Vector2.up.RotateCw(Random.Range(0f, Mathf.PI * 2)) * 0.5f);
        }

        Velocity += (Velocity - (Velocity * FrictionRatio)) * Time.fixedDeltaTime; // 
        Velocity += SteerForces * Time.fixedDeltaTime; //Apply steering forces
        if (SteerForces == Vector2.zero)
        {
            Velocity = Vector2.zero;
        }

        //Clamp the velocity
        if (Velocity.sqrMagnitude > Definition.SqrMaxSpeed)
        {
            Velocity = Velocity.normalized * Definition.MaxSpeed;
        }

        movement_vector += Velocity;

        //Move the Creep
        Rigidbody.MovePosition(position + movement_vector * Time.fixedDeltaTime);
        if (movement_vector.x > 0) m_spriteRenderer.flipX = false;
        if (movement_vector.x < 0) m_spriteRenderer.flipX = true;
    }

    private void ReturnToPool()
    {
        GameSession.FreeInstance(this);
        CreepDestroyed?.Invoke(this);
    }

    public void Damage(int amount)
    {
        if( amount == int.MaxValue )
        {
            var explosion = GameSession.AllocateInstance(Definition.ExplosionEffect);
            explosion.transform.position = transform.position;
        }

        if (gameObject.activeSelf)
        {
            HitPoints -= amount;
            if (HitPoints <= 0)
            {
                ReturnToPool();
            }
        }
    }

#if UNITY_EDITOR
    private void OnDrawGizmosSelected()
    {
        HexGrid grid = GameSession.Instance?.HexGrid;
        Gizmos.DrawLine(transform.position, transform.position + new Vector3(SteerForces.x, SteerForces.y, 0));
        if (grid && Path.Count > 0)
        {
            Gizmos.color = Color.yellow;
            foreach (Vector2 path_point in Path)
            {
                Gizmos.DrawCube(path_point, Vector3.one * 0.2f);
            }
        }
    }
#endif
}
