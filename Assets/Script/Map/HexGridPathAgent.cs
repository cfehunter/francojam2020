﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;

public class HexGridPathAgent : MonoBehaviour
{
    public HexGrid m_HexGrid;
    public List<Vector2Int> m_Path = new List<Vector2Int>();
    public Vector2Int m_Position;
    public Vector2Int m_To;

    public bool RebuildPath()
    {
        m_Path.Clear();
        return m_HexGrid.FindPath(m_Position, m_To, m_Path);
    }

#if UNITY_EDITOR
    private void OnDrawGizmosSelected()
    {
        if (m_HexGrid != null && m_Position != m_To)
        {
            Gizmos.color = Color.red;
            Gizmos.DrawSphere(m_HexGrid.GetWorldPosition(m_Position), 0.5f);

            Gizmos.color = Color.green;
            Gizmos.DrawSphere(m_HexGrid.GetWorldPosition(m_To), 0.5f);

            Gizmos.color = Color.yellow;
            foreach (Vector2Int path_point in m_Path)
            {
                Gizmos.DrawCube(m_HexGrid.GetWorldPosition(path_point), Vector3.one * 0.2f);
            }
        }
    }
#endif

}
