﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;

[CreateAssetMenu(menuName = "Franco Jam/Pathfinding Hex")]
public class PathfindingHex : Tile
{
    public HexType Type;
}
