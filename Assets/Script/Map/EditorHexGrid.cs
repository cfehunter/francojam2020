﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEditor;
using UnityEngine.Tilemaps;

#if UNITY_EDITOR
public partial class HexGrid
{
    public Tile            m_ImpassableHex;
    public Tile            m_PassableHex;
    public ContactFilter2D m_ImpassableFilter = new ContactFilter2D();

    public void BakeImpassable()
    {
        Tilemap tilemap = GetComponent<Tilemap>();
        BoxCollider2D grid_collider = GetComponent<BoxCollider2D>();

        if (grid_collider == null || tilemap == null)
        {
            Debug.LogError($"[{name}] Expected Tilemap and BoxCollider2D Components");
            return;
        }

        if (m_ImpassableHex == null)
        {
            Debug.LogError("Impassable Hex Not Set");
            return;
        }

        if (m_PassableHex == null)
        {
            Debug.LogError("Passable Hex Not Set");
            return;
        }

        if (tilemap.cellSize.x != tilemap.cellSize.y)
        {
            Debug.LogError("Cell Width/Height isn't equal.");
            return;
        }

        float cell_radius = tilemap.cellSize.x;
        float half_cell_radius = cell_radius * 0.5f;
        float sqr_half_cell_radius = half_cell_radius * half_cell_radius;

        Vector2 min_bounds = grid_collider.bounds.min;
        Vector2 max_bounds = grid_collider.bounds.max;
        Vector3Int min_cell = tilemap.WorldToCell(min_bounds);
        Vector3Int max_cell = tilemap.WorldToCell(max_bounds);

        tilemap.ClearAllTiles();

        tilemap.size = max_cell - min_cell;

        //Fill with impassable
        tilemap.BoxFill(min_cell, m_ImpassableHex, min_cell.x, min_cell.y, max_cell.x, max_cell.y);

        //Fill the inner area with passable
        ++min_cell.x;
        ++min_cell.y;
        --max_cell.x;
        --max_cell.y;
        tilemap.BoxFill(min_cell, m_PassableHex, min_cell.x, min_cell.y, max_cell.x, max_cell.y);

        //Collect and filter all impassable colliders
        List<Collider2D> colliders_on_grid = new List<Collider2D>();
        Physics2D.OverlapCollider(grid_collider, m_ImpassableFilter, colliders_on_grid);

        IEnumerable<Collider2D> impassable_colliders = from c in colliders_on_grid where c.gameObject.isStatic select c;
        foreach (Collider2D impassable in impassable_colliders)
        {
            min_cell = tilemap.WorldToCell(impassable.bounds.min);
            max_cell = tilemap.WorldToCell(impassable.bounds.max);
            min_cell.z = max_cell.z = 0;

            for (Vector3Int cell = min_cell; cell.y <= max_cell.y; ++cell.y)
            {
                for (cell.x = min_cell.x; cell.x <= max_cell.x; ++cell.x)
                {
                    Vector2 cell_world_center = tilemap.GetCellCenterWorld(cell);

                    if ((cell_world_center - impassable.ClosestPoint(cell_world_center)).sqrMagnitude <= sqr_half_cell_radius)
                    {
                        tilemap.SetTile(cell, m_ImpassableHex);
                    }
                }
            }
        }
    }

    public void ClearHexes()
    {
        GetComponent<Tilemap>().ClearAllTiles();
    }

    [MenuItem("GameObject/Franco Jam/Hex Grid")]
    static void CreateHexGrid()
    {
        GameObject new_hex_grid_object = new GameObject("Hex Grid", typeof(HexGrid));
        HexGrid new_hex_grid = new_hex_grid_object.GetComponent<HexGrid>();
        new_hex_grid.m_ImpassableHex = AssetDatabase.LoadAssetAtPath<Tile>("Assets/Core/Pathfinding/ImpassableHex.asset");
        new_hex_grid.m_PassableHex = AssetDatabase.LoadAssetAtPath<Tile>("Assets/Core/Pathfinding/PassableHex.asset");
        new_hex_grid.m_ImpassableFilter = new ContactFilter2D();
        new_hex_grid.m_ImpassableFilter.layerMask = Physics2D.AllLayers;

        new_hex_grid_object.GetComponent<BoxCollider2D>().size = new Vector2(10f, 10f);
        new_hex_grid_object.GetComponent<Tilemap>().tileAnchor = Vector3.zero;

        Grid new_grid = new_hex_grid_object.GetComponent<Grid>();
        new_grid.cellLayout = GridLayout.CellLayout.Hexagon;
        new_grid.cellSwizzle = GridLayout.CellSwizzle.YXZ;
    }

}
#endif