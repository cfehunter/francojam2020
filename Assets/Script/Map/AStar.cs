﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//Pete Note: I'm assuming uniform costs for now
public static class AStar
{
    class AStarCell : IComparable<AStarCell>
    {
        public AStarCell  Previous = null;
        public Vector2Int Position;
        public int        DistanceFromStart;
        public int        EstimatedDistanceToEnd;

        public int CompareTo(AStarCell rhs)
        {
            return (rhs.DistanceFromStart + rhs.EstimatedDistanceToEnd).CompareTo(DistanceFromStart + EstimatedDistanceToEnd);
        }
    }

    static void add_cell_ordered(List<AStarCell> cells, AStarCell new_cell)
    {
        int index = cells.BinarySearch(new_cell);

        if (index < 0)
        {
            index = ~index;
        }

        cells.Insert(index, new_cell);
    }

    public static bool FindPath(HexGrid grid, Vector2Int from, Vector2Int to, List<Vector2Int> result)
    {
        //Already there
        if (from == to)
            return true;

        //Can't reach
        if (grid[from] == HexType.Blocked || grid[to] == HexType.Blocked)
            return false;


        Dictionary<Vector2Int, AStarCell> visited_cells = new Dictionary<Vector2Int, AStarCell>();
        List<AStarCell> open_list = new List<AStarCell>();

        AStarCell dest_result = null;
        AStarCell new_cell = new AStarCell { Position = from };
        visited_cells[from] = new_cell;
        open_list.Add(new_cell);

        AdjacentHex[] adjacent_cells = new AdjacentHex[6];

        while (open_list.Count > 0)
        {
            AStarCell prev_cell = open_list.PopBack();
            if (prev_cell.Position == to)
            {
                dest_result = prev_cell;
                break;
            }

            //Expand and add adjacent hexes
            grid.GetAdjacent(prev_cell.Position, adjacent_cells);
            foreach (AdjacentHex adjacent in adjacent_cells)
            {
                if (adjacent.Type != HexType.Blocked)
                {

                    int distance_from_start = prev_cell.DistanceFromStart + 1;
                    if (visited_cells.TryGetValue(adjacent.Position, out AStarCell existing))
                    {
                        //Update the existing if we have a more efficient path to it and add it back to the open list
                        if (existing.DistanceFromStart > distance_from_start)
                        {
                            existing.DistanceFromStart = distance_from_start;
                            existing.Previous = prev_cell;
                            add_cell_ordered(open_list, existing);
                        }
                    }
                    else
                    {
                        //Unvisited node. Add it
                        new_cell = new AStarCell
                        {
                            Previous = prev_cell,
                            Position = adjacent.Position,
                            DistanceFromStart = distance_from_start,
                            EstimatedDistanceToEnd = Mathf.FloorToInt((to - adjacent.Position).magnitude)
                        };

                        visited_cells.Add(adjacent.Position, new_cell);
                        add_cell_ordered(open_list, new_cell);
                    }
                }
            }
        }
        
        if (dest_result != null)
        {
            //Reuse the open list buffer to reverse the path result
            open_list.Clear();
            for (AStarCell cell = dest_result; cell.Position != from; cell = cell.Previous)
            {
                open_list.Add(cell);
            }

            open_list.Reverse();
            foreach (AStarCell cell in open_list)
            {
                result.Add(cell.Position);
            }

            return true;
        }

        return false;
    }
}
