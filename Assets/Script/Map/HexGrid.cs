﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEditor;
using UnityEngine;
using UnityEngine.Tilemaps;

public enum HexType : byte
{
     Passable
    ,Blocked
}

public struct AdjacentHex
{
    public AdjacentHex(int x, int y, HexType Type)
        : this(new Vector2Int(x,y), Type)
    { }

    public AdjacentHex(Vector2Int Position, HexType Type)
    {
        this.Position = Position;
        this.Type = Type;
    }

    public Vector2Int Position;
    public HexType    Type;
}

[RequireComponent(typeof(Tilemap), typeof(TilemapRenderer), typeof(BoxCollider2D))]
[RequireComponent(typeof(Grid))]
public partial class HexGrid : MonoBehaviour
{
    private Tilemap m_Tilemap;
    public BoundsInt CellBounds => m_Tilemap.cellBounds;

    private void Awake()
    {
        m_Tilemap = GetComponent<Tilemap>();

        GetComponent<TilemapRenderer>().enabled = false;
        GetComponent<BoxCollider2D>().enabled = false;
    }

    public AdjacentHex[] GetAdjacent(Vector2Int origin)
    {
        AdjacentHex[] result = new AdjacentHex[6];
        GetAdjacent(origin, result);
        return result;
    }

    public void GetAdjacent(Vector2Int origin, AdjacentHex[] result)
    {
        //Rows are jagged, so odd/even rows have different adjacent indices for the top/bottom rows
        if (origin.x % 2 == 0)
        {
            result[0] = new AdjacentHex(origin.x, origin.y + 1,     this[origin.x, origin.y + 1]); //Top
            result[1] = new AdjacentHex(origin.x, origin.y - 1,     this[origin.x, origin.y - 1]); //Bottom
            result[2] = new AdjacentHex(origin.x - 1, origin.y,     this[origin.x - 1, origin.y]); //Top Left
            result[3] = new AdjacentHex(origin.x + 1, origin.y,     this[origin.x + 1, origin.y]); //Top Right
            result[4] = new AdjacentHex(origin.x - 1, origin.y - 1, this[origin.x - 1, origin.y - 1]); //Bottom Left
            result[5] = new AdjacentHex(origin.x + 1, origin.y - 1, this[origin.x + 1, origin.y - 1]); //Bottom Right
        }
        else
        {
            result[0] = new AdjacentHex(origin.x, origin.y + 1,     this[origin.x, origin.y + 1]); //Top
            result[1] = new AdjacentHex(origin.x, origin.y - 1,     this[origin.x, origin.y - 1]); //Bottom
            result[2] = new AdjacentHex(origin.x - 1, origin.y + 1, this[origin.x - 1, origin.y + 1]); //Top Left
            result[3] = new AdjacentHex(origin.x + 1, origin.y + 1, this[origin.x + 1, origin.y + 1]); //Top Right
            result[4] = new AdjacentHex(origin.x - 1, origin.y,     this[origin.x - 1, origin.y]); //Bottom Left
            result[5] = new AdjacentHex(origin.x + 1, origin.y,     this[origin.x + 1, origin.y]); //Bottom Right
        }
    }

    public bool FindPath(Vector2Int from, Vector2Int to, List<Vector2Int> path)
    {
        return AStar.FindPath(this, from, to, path);
    }

    public Vector3 GetWorldPosition(Vector2Int cell)
    {
        return m_Tilemap.GetCellCenterWorld(new Vector3Int(cell.y, cell.x, 0));
    }

    public Vector2Int GetCellPosition(Vector3 world)
    {
        Vector3Int cell_pos = m_Tilemap.WorldToCell(world);
        return new Vector2Int(cell_pos.y, cell_pos.x);
    }

    public HexType GetHexType(Vector2Int cell)
    {
        return this[cell.x, cell.y];
    }

    public HexType GetHexType(int x, int y)
    {
        return this[x, y];
    }

    public HexType this[Vector2Int cell]
    {
        get { return this[cell.x, cell.y]; }
        set { this[cell.x, cell.y] = value; }
    }

    public HexType this[int x, int y]
    {
        get
        {
            PathfindingHex tile = m_Tilemap.GetTile<PathfindingHex>(new Vector3Int(y, x, 0));
            return tile?.Type ?? HexType.Blocked;
        }
        set
        {
            PathfindingHex hex = m_Tilemap.GetTile<PathfindingHex>(new Vector3Int(y, x, 0));
            if (hex)
                hex.Type = value;
        }
    }
}
