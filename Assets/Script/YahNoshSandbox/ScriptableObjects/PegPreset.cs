﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "YahNosh/Sandbox/PegPreset")]
public class PegPreset : ScriptableObject
{
    public string Label;

    public Color IdleColor;
    public Color PoweredColor;
    public Color SelectedColor;
    public Color ActivatedColor;
}
