﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PegComponent : MonoBehaviour
{
    public static List<PegComponent> InstanceList = new List<PegComponent>();

    public PegPreset Preset;

    [SerializeField]
    private SpriteRenderer Peg;

    [SerializeField]
    private Text Label;

    public enum PegState { Idle, Selected, Activated };
    private PegState _State;

    public List<PegComponent> Connections;
    
    private bool _GraphNodeVisited = false;
    private bool _Powered = false;

    public PegState State
    {
        get { return _State; }
        set { _State = value; StateChanged(); }
    }

    private void Awake()
    {
        InstanceList.Add(this);
    }

    private void OnDestroy()
    {
        InstanceList.Remove(this);
    }

    public bool GraphNodeVisited
    {
        get { return _GraphNodeVisited; }
        set { _GraphNodeVisited = value; }
    }

    public bool Powered
    {
        get { return _Powered; }
        set { _Powered = value; StateChanged(); }
    }

    // Start is called before the first frame update
    void Start()
    {
        // Set the peg label
        Label.text = Preset.Label;

        State = PegState.Idle;

        Connections = new List<PegComponent>();
    }

    public void StateChanged()
    {
        switch (_State)
        {
            case PegState.Idle:
                if (_Powered)
                {
                    Peg.color = Preset.PoweredColor;
                }
                else
                {
                    Peg.color = Preset.IdleColor;
                }
                break;
            case PegState.Selected:
                Peg.color = Preset.SelectedColor;
                break;
            case PegState.Activated:
                Peg.color = Preset.ActivatedColor;
                break;
        }
    }
}
