﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WorldScript : MonoBehaviour
{
    private struct DFSStruct
    {
        public DFSStruct(PegComponent peg, int currentIndex)
        {
            Peg = peg;
            CurrentIndex = currentIndex;
        }

        public PegComponent Peg;
        public int CurrentIndex;
    }

    [SerializeField]
    private float ScrollSpeed = 1.0f;

    [SerializeField]
    private GameObject MainBasePrefab;

    // TODO: currently just instantly connects what you click on, need to implement "dragging" the connection to the target
    //[SerializeField]
    //private LineRenderer NewConnectionRenderer;

    [SerializeField]
    private GameObject ConnectionRendererPrefab;

    private Vector2 CurrentCameraVelocity;

    private GameObject MainBasePegObj;
    private PegComponent MainBasePeg;

    private List<GameObject> ConnectionRendererObjList;
    private List<LineRenderer> ConnectionRendererList;
    private List<int> ConnectionRendererFreeList; // In case any renderers run out of lines, we want to be able to reuse them

    private bool HasLoop = false;

    PegComponent SelectedPeg;

    // Start is called before the first frame update
    void Start()
    {
        // Create the main base
        MainBasePegObj = Instantiate(MainBasePrefab, new Vector3(0, 0, 0), Quaternion.identity);
        MainBasePeg = MainBasePegObj.GetComponent<PegComponent>();
        MainBasePeg.Powered = true;

        ConnectionRendererObjList = new List<GameObject>();
        ConnectionRendererList = new List<LineRenderer>();
        ConnectionRendererFreeList = new List<int>();

        CurrentCameraVelocity = new Vector2(0, 0);
        SelectedPeg = null;

        // Create an initial connection renderer
        GetNewConnectionRenderer();
    }

    // Update is called once per frame
    void Update()
    {
        HandleMouse();
        HandleCameraMovement();
    }

    private void FixedUpdate()
    {
        Vector3 newCamPos = Camera.main.transform.position;
        Vector2 deltaPos = CurrentCameraVelocity * Time.deltaTime;
        
        newCamPos.x += deltaPos.x;
        newCamPos.y += deltaPos.y;

        Camera.main.transform.position = newCamPos;
    }

    private void HandleMouse()
    {
        // Either LClick, or RClick while a peg is selected
        if (Input.GetMouseButtonUp(0) || Input.GetMouseButtonUp(1))
        {
            HandleClick();
        }
    }

    private void HandleCameraMovement()
    {
        CurrentCameraVelocity.Set(0, 0);

        if (Input.GetKey(KeyCode.W))
        {
            CurrentCameraVelocity.y = ScrollSpeed;
        }
        else if(Input.GetKey(KeyCode.S))
        {
            CurrentCameraVelocity.y = -ScrollSpeed;
        }

        if(Input.GetKey(KeyCode.A))
        {
            CurrentCameraVelocity.x = -ScrollSpeed;
        }
        else if(Input.GetKey(KeyCode.D))
        {
            CurrentCameraVelocity.x = ScrollSpeed;
        }
    }

    private void HandleClick()
    {
        Vector2 mousePos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        RaycastHit2D hit = Physics2D.Raycast(mousePos, Vector2.zero);

        if (hit.collider)
        {
            PegComponent peg = hit.collider.gameObject.GetComponent<PegComponent>();
            if(peg)
            {
                PegHit(peg);
                return;
            }
        }

        Deselect();
    }

    private void PegHit(PegComponent peg)
    {
        if(HasLoop)
        {
            BreakCycle();
            return;
        }

        if(peg == SelectedPeg)
        {
            Deselect();
            return;
        }

        if (Input.GetMouseButtonUp(0))
        {
            if (SelectedPeg && (SelectedPeg != peg))
            {
                SelectedPeg.State = PegComponent.PegState.Idle;
            }

            peg.State = PegComponent.PegState.Selected;
            SelectedPeg = peg;
        }
        else if (Input.GetMouseButtonUp(1) && SelectedPeg)
        {
            // First make sure the connection is valid (i.e not between already connected pegs)
            if ((SelectedPeg == peg) || SelectedPeg.Connections.Contains(peg))
            {
                return;
            }

            if (!SelectedPeg.Powered)
            {
                // Cannot connect from unpowered pegs
                return;
            }

            // Test the connection for intersections
            if (TestNewConnection(SelectedPeg.transform.position, peg.transform.position))
            {
                // Valid connection, check for loops (without connecting the two just yet)
                CheckLoop(SelectedPeg, peg);

                // Link up the pegs
                SelectedPeg.Connections.Add(peg);
                peg.Connections.Add(SelectedPeg);

                // Rebuild the graph based on the new topology
                // NOTE: yes this is pretty bad, but I'm only using it because LineRenderer prefers paths and not arbitrary sets of line segments
                // In the real thing, we will have some custom renderer anyway
                RebuildGraph();
            }
        }
    }

    private int GetNewConnectionRenderer()
    {
        if(ConnectionRendererFreeList.Count > 0)
        {
            // Retrieve from the free list
            return ConnectionRendererFreeList.PopBack();
        }
        else
        {
            // Have to create a new line renderer
            GameObject newConnectionRendererObj = Instantiate(ConnectionRendererPrefab);
            ConnectionRendererObjList.Add(newConnectionRendererObj);

            LineRenderer newConnectionRenderer = newConnectionRendererObj.GetComponent<LineRenderer>();
            newConnectionRenderer.positionCount = 0;
            ConnectionRendererList.Add(newConnectionRenderer);
            return (ConnectionRendererList.Count - 1);
        }
    }

    private void RebuildGraph()
    {
        // Reset the free list
        ConnectionRendererFreeList.Clear();

        // Set all pegs to be unvisited so they are visited by DFS
        foreach(PegComponent currentPeg in PegComponent.InstanceList)
        {
            currentPeg.Powered = false;
            currentPeg.GraphNodeVisited = false;
        }

        // The main base is always powered
        MainBasePeg.Powered = true;

        // Reset all line renderers and add them to the free list
        int currentRendererIndex = 0;
        foreach (LineRenderer currentRenderer in ConnectionRendererList)
        {
            currentRenderer.positionCount = 0;
            ConnectionRendererFreeList.Add(currentRendererIndex++);
        }

        // First run the rebuild DFS on the main base peg
        DepthFirstBuildGraph(MainBasePeg);

        // Go over our node list to make sure we didn't miss any isolated nodes
        foreach(PegComponent currentPeg in PegComponent.InstanceList)
        {
            // Only accept nodes that haven't been visited already and have exactly one connection (this would make them an isolated "endpoint" node, causes less strain for the line renderer)
            if(currentPeg.GraphNodeVisited || (currentPeg.Connections.Count != 1))
            {
                continue;
            }

            // Build from the new "root" (assumed to be an isolated node)
            DepthFirstBuildGraph(currentPeg);
        }
    }

    private void DepthFirstBuildGraph(PegComponent rootPeg)
    {
        List<DFSStruct> DFSQueue = new List<DFSStruct>();
        DFSQueue.Add(new DFSStruct(rootPeg, GetNewConnectionRenderer()));

        while (DFSQueue.Count > 0)
        {
            DFSStruct currentNode = DFSQueue.PopBack();
            currentNode.Peg.GraphNodeVisited = true;

            int renderedConnections = 0;
            foreach (PegComponent connectedPeg in currentNode.Peg.Connections)
            {
                if(connectedPeg.GraphNodeVisited)
                {
                    continue;
                }

                int lineRendererIndex = currentNode.CurrentIndex;
                if (renderedConnections > 0)
                {
                    // Node has multiple outgoing edges, i.e branching, so the next edge needs to be drawn by a new line renderer (blame Unity)
                    lineRendererIndex = GetNewConnectionRenderer();
                }

                // Add the graph edge                
                AddGraphEdge(lineRendererIndex, currentNode.Peg, connectedPeg);

                // Transfer power through connections
                connectedPeg.Powered = currentNode.Peg.Powered;

                // Append node to DFS queue if it hasn't been visited yet
                DFSQueue.Add(new DFSStruct(connectedPeg, lineRendererIndex));

                // Increment the number of branches
                ++renderedConnections;
            }
        }
    }

    private void AddGraphEdge(int rendererIndex, PegComponent sourcePeg, PegComponent destPeg)
    {
        LineRenderer currentConnectionRenderer = ConnectionRendererList[rendererIndex];
        if(currentConnectionRenderer.positionCount == 0)
        {
            // Initializing an empty line renderer, add the source point as well
            ++currentConnectionRenderer.positionCount;
            currentConnectionRenderer.SetPosition(0, sourcePeg.transform.position);
        }

        ++currentConnectionRenderer.positionCount;
        currentConnectionRenderer.SetPosition(currentConnectionRenderer.positionCount - 1, destPeg.transform.position);
    }
   
    private void CheckLoop(PegComponent pegA, PegComponent pegB)
    {
        // Set all pegs to be unvisited
        foreach (PegComponent currentPeg in PegComponent.InstanceList)
        {
            currentPeg.GraphNodeVisited = false;
        }

        // Run a DFS from A, ignoring the direct connection between them. Check if we can find out way to B (if so, we have discovered the loop)
        List<DFSStruct> DFSQueue = new List<DFSStruct>();

        DFSQueue.Add(new DFSStruct(pegA, 0));

        while(DFSQueue.Count > 0)
        {
            DFSStruct currentNode = DFSQueue[DFSQueue.Count - 1];
            currentNode.Peg.GraphNodeVisited = true;

            if (currentNode.CurrentIndex >= currentNode.Peg.Connections.Count)
            {
                // No more connections to check from this node, remove from the queue
                DFSQueue.PopBack();
                continue;
            }

            PegComponent currentConnectedPeg = currentNode.Peg.Connections[currentNode.CurrentIndex];
            if(currentConnectedPeg == pegB)
            {
                // Found our way to B, so we definitely have a loop
                pegB.State = PegComponent.PegState.Activated;

                // The current node stack gives us all the nodes in the loop
                foreach (DFSStruct node in DFSQueue)
                {
                    node.Peg.State = PegComponent.PegState.Activated;
                }

                // Set this one separately since we popped it from the stack
                currentNode.Peg.State = PegComponent.PegState.Activated;
                HasLoop = true;
                return;
            }

            // Increment the index so we inspect the next connection if the stack returns here
            ++currentNode.CurrentIndex;
            DFSQueue[DFSQueue.Count - 1] = currentNode;

            if (!currentConnectedPeg.GraphNodeVisited)
            {
                // Add the child we were inspecting to the queue and keep searching recursively
                DFSQueue.Add(new DFSStruct(currentConnectedPeg, 0));
            }
        }
    }

    private void BreakCycle()
    {
        HasLoop = false;

        // Go over whole graph, find all the Activated nodes, and remove any connections to other activated nodes
        foreach (PegComponent currentPeg in PegComponent.InstanceList)
        {
            if (currentPeg.State == PegComponent.PegState.Activated)
            {
                currentPeg.Connections.RemoveAll(connectedPeg => (connectedPeg.State == PegComponent.PegState.Activated));
            }
        }

        // Separate cycle to actually unset all activated nodes
        foreach (PegComponent currentPeg in PegComponent.InstanceList)
        {
            if (currentPeg.State == PegComponent.PegState.Activated)
            {
                currentPeg.State = PegComponent.PegState.Idle;
            }
        }

        // Cycle is removed from the graph, rebuild it
        RebuildGraph();
    }

    private bool TestNewConnection(Vector3 newConnectionA, Vector3 newConnectionB)
    {
        // Test the new connection we are drawing against all the previous connections
        foreach(LineRenderer currentRenderer in ConnectionRendererList)
        {
            if(currentRenderer.positionCount == 0)
            {
                continue;
            }

            for (int nodeIndex = 0; nodeIndex < currentRenderer.positionCount - 1; ++nodeIndex)
            {
                Vector3 currentConnectionA = currentRenderer.GetPosition(nodeIndex);
                Vector3 currentConnectionB = currentRenderer.GetPosition(nodeIndex + 1);

                if (AreLinesIntersecting(newConnectionA, newConnectionB, currentConnectionA, currentConnectionB))
                {
                    // Found a self-intersection, do not permit this connection
                    return false;
                }
            }
        }
        return true;
    }

    private bool AreLinesIntersecting(Vector3 l1_p1, Vector3 l1_p2, Vector3 l2_p1, Vector3 l2_p2)
    {
        // For float precision:
        float epsilon = 0.00001f;

        // Check if lines are parallel
        float denominator = (l2_p2.y - l2_p1.y) * (l1_p2.x - l1_p1.x) - (l2_p2.x - l2_p1.x) * (l1_p2.y - l1_p1.y);
        if (Mathf.Abs(denominator) > 0.0f)
        {
            float u_a = ((l2_p2.x - l2_p1.x) * (l1_p1.y - l2_p1.y) - (l2_p2.y - l2_p1.y) * (l1_p1.x - l2_p1.x)) / denominator;
            float u_b = ((l1_p2.x - l1_p1.x) * (l1_p1.y - l2_p1.y) - (l1_p2.y - l1_p1.y) * (l1_p1.x - l2_p1.x)) / denominator;

            // Is intersecting if u_a and u_b are between 0 and 1
            if ((u_a > (0f + epsilon)) && (u_a < (1f - epsilon)) && (u_b > (0f + epsilon)) && (u_b < (1f - epsilon)))
            {
                return true;
            }
        }

        return false;
    }

    private void Deselect()
    {
        if(SelectedPeg)
        {
            SelectedPeg.State = PegComponent.PegState.Idle;
            SelectedPeg = null;
        }
    }
}
