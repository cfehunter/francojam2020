﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DeferredCallbacks
{
    List<Action>          m_InvocationList = new List<Action>();
    List<(Action, float)> m_PendingCallbacks = new List<(Action, float)>();

    public void AddCallback(Action action, float delay)
    {
        m_PendingCallbacks.Add((action, delay));
    }

    public void AddCallback(Action action)
    {
        AddCallback(action, 0f);
    }

    public void Update(float delta_time)
    {
        if (m_PendingCallbacks.Count > 0)
        {
            //Update the timers and move expired callbacks over to the invocation list
            m_InvocationList.Clear();
            for (int i = m_PendingCallbacks.Count - 1; i >= 0; --i)
            {
               var (action, time_remaining) = m_PendingCallbacks[i];
                if (time_remaining <= delta_time)
                {
                    m_InvocationList.Add(action);
                    m_PendingCallbacks.QuickRemoveAt(i);
                }
                else
                {
                    m_PendingCallbacks[i] = (action, time_remaining - delta_time);
                }
            }

            //Call of the expired callbacks
            foreach (Action a in m_InvocationList)
            {
                a();
            }
            m_InvocationList.Clear();
        }
    }
}
