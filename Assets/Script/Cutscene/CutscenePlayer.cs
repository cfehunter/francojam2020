﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CutscenePlayer : MonoBehaviour
{
    public static event Action<DialogLine> OnCutsceneDialogChanged;

    [SerializeField]
    CutsceneDefinition m_Definition;
    int                m_CurrentDialogIndex;

    public CutsceneDefinition Definition         => m_Definition;
    public bool               Completed          => !m_Definition || m_Definition.DialogLines.Count <= m_CurrentDialogIndex;
    public int                CurrentDialogIndex => m_CurrentDialogIndex;
    public DialogLine         CurrentDialogLine  => Completed ? null : m_Definition.DialogLines[m_CurrentDialogIndex];

    public void PlayCutscene()
    {
        m_CurrentDialogIndex = 0;
        StartLine();
    }

    private void Start()
    {
        if (Definition)
            PlayCutscene();
    }

    private void Update()
    {
        if (Input.GetButtonDown("Submit"))
        {
            EndLine();
            StartLine();
        }

        if (Completed)
        {
            enabled = false;
        }
    }

    private void StartLine()
    {
        OnCutsceneDialogChanged?.Invoke(CurrentDialogLine);
    }

    private void EndLine()
    {
        ++m_CurrentDialogIndex;
    }
}
