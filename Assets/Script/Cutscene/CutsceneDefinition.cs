﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Franco Jam/Cutscene")]
public class CutsceneDefinition : ScriptableObject
{
    [SerializeField] List<DialogLine> m_DialogLines = new List<DialogLine>();
    public IReadOnlyList<DialogLine>  DialogLines => m_DialogLines;
}
