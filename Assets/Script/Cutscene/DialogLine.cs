﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class DialogLine
{
    public string     NameText;
    public Color      NameColour = Color.white;
    public string     ContentText;
    public Color      ContentColour = Color.white;
    public TextAnchor ContentAlignment = TextAnchor.UpperLeft;
    public Sprite     Portrait;
    public AudioClip  Audio;
}
