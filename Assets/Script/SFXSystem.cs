﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SFXSystem : MonoBehaviour
{
    [SerializeField] private SFXPool[] _pools;

    void Awake()
    {
        DefenseCore.DefenseCoreDamaged += OnDefenseCoreDamaged;
        DefenseCore.DefenseCoreLowHealth += OnDefenseCoreLowHealth;
        Creep.CreepDestroyed += OnCreepDestroyed;
        TurretWeapon.WeaponFired += OnWeaponFired;
        TurretWall.ReceiveDamage += OnTurretWallReceiveDamage;
        TurretWall.LinkStarted += OnLinkStarted;
        TurretWall.LinkCompleted += OnLinkCompleted;
        TurretWall.WallDestroyed += OnWallDestroyed;
        GameSession.StateChanged += OnGameStateChanged;
        GameSession.WaveCompleted += OnWaveCompleted;
        GameSession.WaveStarted += OnWaveStarted;
        TurretBehaviour.WallLoopCreated += OnWallLoopCreated;
        TurretBehaviour.UsedGridPowerChanged += OnUsedGridPowerChanged;
    }

    void OnDestroy()
    {
        DefenseCore.DefenseCoreDamaged -= OnDefenseCoreDamaged;
        DefenseCore.DefenseCoreLowHealth -= OnDefenseCoreLowHealth;
        Creep.CreepDestroyed -= OnCreepDestroyed;
        TurretWeapon.WeaponFired -= OnWeaponFired;
        TurretWall.ReceiveDamage -= OnTurretWallReceiveDamage;
        TurretWall.LinkStarted -= OnLinkStarted;
        TurretWall.LinkCompleted -= OnLinkCompleted;
        TurretWall.WallDestroyed -= OnWallDestroyed;
        GameSession.StateChanged -= OnGameStateChanged;
        GameSession.WaveCompleted -= OnWaveCompleted;
        GameSession.WaveStarted -= OnWaveStarted;
        TurretBehaviour.WallLoopCreated -= OnWallLoopCreated;
        TurretBehaviour.UsedGridPowerChanged -= OnUsedGridPowerChanged;
    }
   
    private void OnLinkCompleted(TurretWall obj)
    {
        TriggerSFX(SoundId.CompleteLink);
    }

    private void OnLinkStarted(TurretWall obj)
    {
        TriggerSFX(SoundId.StartLink);
    }

    private void OnWeaponFired(TurretWeapon weapon)
    {
        TriggerSFX(weapon.Definition.SoundId);
    }

    private void OnCreepDestroyed(Creep obj)
    {
        TriggerSFX(SoundId.CreepDestroyed);
    }

    private void OnDefenseCoreDamaged(DefenseCore obj)
    {
        TriggerSFX(SoundId.CoreDamaged);
        TriggerSFX(SoundId.CoreUnderAttack);
    }

    private void OnTurretWallReceiveDamage(TurretWall obj)
    {
        TriggerSFX(SoundId.ShieldDamaged);
    }

    private void OnGameStateChanged(GameState state)
    {
        switch (state)
        {
            case GameState.Won:
                TriggerSFX(SoundId.Win);
                break;
            case GameState.Lost:
                TriggerSFX(SoundId.Defeated);
                break;
        }
    }

    private void OnDefenseCoreLowHealth(DefenseCore obj)
    {
        TriggerSFX(SoundId.CoreHealthLow);
    }

    private void OnWaveStarted(GameWaveDefinition wave, int wave_index)
    {
        if (wave_index < GameSession.Instance.Waves.Count - 1)
            TriggerSFX(SoundId.WaveStart);
        else
            TriggerSFX(SoundId.WaveStartFinal);
    }

    private void OnWaveCompleted(GameWaveDefinition wave, int wave_index)
    {
        TriggerSFX(SoundId.WaveComplete);
    }

    private void OnWallLoopCreated(bool enemiesDamaged)
    {
        if (enemiesDamaged)
        {
            TriggerSFX(SoundId.WallLoopCreated);
        }
    }

    private void OnWallDestroyed(TurretWall obj)
    {
        TriggerSFX(SoundId.WallDestroyed);
    }

    private void OnUsedGridPowerChanged(int new_power_usage)
    {
        if (new_power_usage > GameSession.Instance.PowerLimit)
        {
            TriggerSFX(SoundId.LowPower);
        }
    }

    private void TriggerSFX(SoundId id)
    {
        foreach (var pool in _pools)
        {
            if (pool.Id == id)
            {
                bool shouldPlay = false;

                //Early out if the pool is on cooldown
                if (pool.RemainingDelay > 0f)
                    continue;

                switch (pool.PlayMode)
                {
                    case PlayMode.TriggerImmediately:
                        shouldPlay = true;
                        break;
                    case PlayMode.PlayAndLoopWhileTriggersAreReceived:
                        shouldPlay = !pool.Source.isPlaying;
                        pool.LoopCooldown = pool.Source.clip.length;
                        break;
                    case PlayMode.IgnoreTriggerIfAlreadyPlaying:
                        shouldPlay = !pool.Source.isPlaying;
                        break;
                }

                if (shouldPlay)
                {
                    pool.RemainingDelay = pool.DelayBetween;
                    pool.Source.pitch = 1f + UnityEngine.Random.value * pool.PitchRandomiseAmount;
                    pool.Source.Play();
                }
            }
        }
    }

    void Update()
    {
        foreach (var pool in _pools)
        {
            pool.LoopCooldown -= Time.deltaTime;
            pool.LoopCooldown = Mathf.Max(0f, pool.LoopCooldown);
            if (pool.PlayMode == PlayMode.PlayAndLoopWhileTriggersAreReceived)
            {
                pool.Source.loop = pool.LoopCooldown > 0f;
            }

            pool.RemainingDelay -= Time.deltaTime;
            pool.RemainingDelay = Mathf.Max(0f, pool.RemainingDelay);
        }
    }

    public enum SoundId
    {
        CreepDestroyed,
        CoreDamaged,
        ShieldDamaged,
        TeslaFired,
        MachineGunFired,
        WaveStart,
        WaveStartFinal,
        WaveComplete,
        CoreHealthLow,
        CoreUnderAttack,
        Defeated,
        Win,
        StartLink,
        CompleteLink,
        WallLoopCreated,
        LowPower,
        WallDestroyed
    }

    [Serializable]
    internal class SFXPool
    {
        public SoundId Id;
        public PlayMode PlayMode;
        public float PitchRandomiseAmount;
        public AudioSource Source;
        public float LoopCooldown { get; set; }
        public float DelayBetween = 0f;
        public float RemainingDelay { get; set; } = 0f;
    }

    internal enum PlayMode
    {
        TriggerImmediately,
        IgnoreTriggerIfAlreadyPlaying,
        PlayAndLoopWhileTriggersAreReceived
    }
}


