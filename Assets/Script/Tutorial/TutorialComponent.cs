﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TutorialComponent : MonoBehaviour
{
    [SerializeField]
    private DataScriptableObject m_data;

    // Start is called before the first frame update
    void Start()
    {
        Debug.Log($"Yo your data contains float value {m_data.SomeFloat:#.##}");
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
