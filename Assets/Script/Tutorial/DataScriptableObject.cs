﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Tutorial/DataScriptableObject")] //This adds this to the context menu for creating assets
public class DataScriptableObject : ScriptableObject
{
    public float SomeFloat;
    public Transform SomeObject;
    [SerializeField] //Exposes private fields to the editor
    private string superSecret;
}
