﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(SpriteRenderer))]
public class SpriteYSort : MonoBehaviour
{
    const float    c_SubSteps = 100f;
    SpriteRenderer m_Sprite;
    public float   m_PivotOffset = 0f;
    public bool    m_Dynamic = true;

    void Awake()
    {
        m_Sprite = GetComponent<SpriteRenderer>();
        SetSortOrder();

        if (!m_Dynamic)
        {
            Destroy(this);
        }
    }

    void Update()
    {
        SetSortOrder();
    }

    //private void OnDrawGizmosSelected()
    //{
    //    SpriteRenderer sprite = GetComponent<SpriteRenderer>();
    //    float x_pivot =  sprite.bounds.center.x;
    //    float y_pivot = sprite.bounds.min.y + m_PivotOffset;
    //    Gizmos.DrawSphere(new Vector2(x_pivot, y_pivot), 0.1f);
    //}

    void SetSortOrder()
    {
        m_Sprite.sortingOrder = SortLevel(m_Sprite.bounds.min.y + m_PivotOffset);
    }

    public static int SortLevel(float y)
    {
        return -Mathf.RoundToInt(y * c_SubSteps);
    }
}
