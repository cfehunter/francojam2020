﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CreepHealthBar : MonoBehaviour
{
    [SerializeField] Gradient m_HealthColour;
    Creep                     m_Creep;
    SpriteRenderer            m_Overlay;
    Renderer[]                m_SubRenderers;
    float                     m_FullWidth = 1f;
    int                       m_DisplayHealth = int.MinValue;

    private void Awake()
    {
        m_Creep = GetComponentInParent<Creep>();
        Debug.Assert(m_Creep);

        m_Overlay = transform.Find("Overlay")?.GetComponent<SpriteRenderer>();
        Debug.Assert(m_Overlay);
        m_FullWidth = m_Overlay.size.x;
        m_SubRenderers = transform.GetComponentsInChildren<Renderer>();
        SetRenderEnabled(false);
    }

    private void Start()
    {
        transform.rotation = Quaternion.identity;
    }

    private void OnEnable()
    {
        transform.rotation = Quaternion.identity;
    }

    private void FixedUpdate()
    {
        int current_hp = m_Creep.HitPoints;
        if (current_hp != m_DisplayHealth)
        {
            //Toggle show/hide if we are now, or were, full health
            int max_hp = m_Creep.MaxHitPoints;
            bool wax_max_hp = max_hp == m_DisplayHealth;
            if (wax_max_hp || current_hp == max_hp)
            {
                SetRenderEnabled(wax_max_hp);
            }

            m_DisplayHealth = current_hp;
            float normalised_health = m_DisplayHealth / (float)max_hp;
            m_Overlay.size = new Vector2(normalised_health * m_FullWidth, m_Overlay.size.y);
            m_Overlay.color = m_HealthColour.Evaluate(normalised_health);
        }
    }

    private void SetRenderEnabled(bool enabled)
    {
        foreach (Renderer renderer in m_SubRenderers)
        {
            renderer.enabled = enabled;
        }
    }
}
