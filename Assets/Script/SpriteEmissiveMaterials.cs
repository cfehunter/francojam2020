﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(SpriteRenderer))]
public class SpriteEmissiveMaterials : MonoBehaviour
{
    static int     s_EmissiveColourID = Shader.PropertyToID("_Emissive");
    static int     s_EmissiveMaskID = Shader.PropertyToID("_EmissiveMask");
    Color          m_OriginalEmissive = Color.white;
    SpriteRenderer m_Sprite;
    public bool    Locked {get; set;}

    public int SortOrder { get => m_Sprite.sortingOrder; set => m_Sprite.sortingOrder = value; }

    void Awake()
    {
        m_Sprite = GetComponent<SpriteRenderer>();
        Debug.Assert(m_Sprite);
        m_OriginalEmissive = m_Sprite.material.GetColor(s_EmissiveColourID);
    }

    private void OnEnable()
    {
        Locked = false;
    }

    public void SetEmissiveColour(Color c)
    {
        if (!Locked)
            m_Sprite.material.SetColor(s_EmissiveColourID, c);
    }

    public void RestoreEmissiveColour()
    {
        if (!Locked)
            m_Sprite.material.SetColor(s_EmissiveColourID, m_OriginalEmissive);
    }

    public void SetEmissiveMask(Texture2D mask)
    {
        m_Sprite.material.SetTexture(s_EmissiveMaskID, mask);
    }
}
