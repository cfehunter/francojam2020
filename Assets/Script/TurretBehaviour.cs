﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using Random = UnityEngine.Random;

[RequireComponent(typeof(SpriteEmissiveMaterials))]
public class TurretBehaviour : MonoBehaviour
{
    private class DFSNode
    {
        // "Depth-first search" node (as the most commonly used graph algorithm here will resemble DFS)
        public DFSNode(TurretBehaviour turret, int index)
        {
            m_turret = turret;
            m_index = index;
        }

        public TurretBehaviour m_turret;
        public int m_index;
    };

    private bool m_Powered = false;
    public bool IsTurretPowered { get => m_Powered || IsTurretCore; set { m_Powered = value; ApplyPowerState(); } }
    [SerializeField] private bool IsTurretCore = false; // Turret is connected to the core, i.e it's always powered
    private TurretWeapon m_Weapon = null;

    private static List<ulong> LoopTurretIDs = new List<ulong>(); //Just to save re-allocating lists
    public static List<TurretBehaviour> TurretInstances = new List<TurretBehaviour>();

    public static int UsedGridPower { get; private set; } = 0;
    public static int BatteryPower  { get; private set; } = 0;
    public static event Action<int> UsedGridPowerChanged;
    public static event Action<bool> WallLoopCreated;

    SpriteEmissiveMaterials m_EmissiveMaterials;

    //
    // "TURRET GRAPH" PROPERTIES
    //

    // Connections to other turrets, which form a graph
    private List<TurretBehaviour> m_ConnectedTurrets = new List<TurretBehaviour>();
    public IReadOnlyList<TurretBehaviour> ConnectedTurrets => m_ConnectedTurrets;

    private bool m_nodeVisited = false; // Used by graph search algorithms
    public bool HasRouteToCore
    {
        get;
        set;
    }
    private bool m_inLoop = false;

    public TurretWeapon[] m_availableWeapons;
    private int m_weaponIndex;

    private void Awake()
    {
        m_EmissiveMaterials = GetComponent<SpriteEmissiveMaterials>();
        m_Weapon = transform.GetComponentInChildren<TurretWeapon>();
        m_animator = GetComponent<Animator>();
        Debug.Assert(m_Weapon);
        TurretInstances.Add(this);
    }

    private void Start()
    {
        FormCoreConnection();
        ApplyPowerState();
        ChangeWeapon(m_availableWeapons[0]);
        m_prompt = transform.Find("prompt");
        m_prompt?.gameObject.SetActive(false);
    }

    public void ToggleWeapon()
    {
        m_weaponIndex = (m_weaponIndex + 1) % m_availableWeapons.Length;
        ChangeWeapon(m_availableWeapons[m_weaponIndex]);
    }

    bool m_wasHighlighted = false;
    bool m_isHighlighted = false;
    Animator m_animator = null;
    Transform m_prompt = null;

    public void HighlightTurret()
    {
        m_isHighlighted = true;
        if (m_wasHighlighted == false)
        {
            m_wasHighlighted = true;
            m_animator.Play("TurretHighlight");
            m_prompt?.gameObject.SetActive(true);
        }
    }

    public void FixedUpdate()
    {
        if(m_wasHighlighted && m_isHighlighted == false)
        {
            m_animator.Play("TurretIdle");
            m_wasHighlighted = false;
            m_prompt?.gameObject.SetActive(false);
        }
        m_isHighlighted = false;
    }

    private void OnDestroy()
    {
        TurretInstances.Remove(this);
    }

    public static TurretBehaviour GetNearestTurret(Vector2 position, float interactionRangeSq)
    {
        if (TurretInstances.Count == 0)
        {
            return null;
        }

        TurretBehaviour nearestTurret = null;
        float nearestDistance = float.MaxValue;
        foreach (var currentTurret in TurretInstances)
        {
            var nextDistance = (currentTurret.transform.position2D() - position).sqrMagnitude;
            if ((nextDistance < interactionRangeSq) && (nextDistance < nearestDistance))
            {
                nearestDistance = nextDistance;
                nearestTurret = currentTurret;
            }
        }

        return nearestTurret;
    }

    public static void ResetGraphVisited()
    {
        // Reset the "visited" state of all nodes
        foreach (TurretBehaviour currentTurret in TurretInstances)
        {
            currentTurret.m_nodeVisited = false;
        }
    }

    public static bool ConnectionIntersectionTest(Vector2 lineStart, Vector2 lineEnd)
    {
        // Set all pegs to be unvisited
        ResetGraphVisited();

        // Iterate through all nodes (order is irrelevant) to check all connections
        // TODO: if this starts to slow things down, could use a circle/AABB to cull any connections that are nowhere near it?
        foreach (TurretBehaviour currentTurret in TurretInstances)
        {
            currentTurret.m_nodeVisited = true;
            foreach (TurretBehaviour connectedTurret in currentTurret.ConnectedTurrets)
            {
                if (connectedTurret.m_nodeVisited)
                {
                    continue;
                }

                Vector2 connectionStart = currentTurret.transform.position2D();
                Vector2 connectionEnd = currentTurret.transform.position2D();
                if (SpatialUtil.LineIntersection(connectionStart, connectionEnd, lineStart, lineEnd))
                {
                    return true;
                }
            }
        }
        return false;
    }

    public static void BreakConnectionBetween(TurretBehaviour from, TurretBehaviour to)
    {
        from.m_ConnectedTurrets.QuickRemove(to);
        to.m_ConnectedTurrets.QuickRemove(from);

        UpdateGridPower();
    }

    public bool IsConnectedToTurret(TurretBehaviour rhs)
    {
        return ((rhs != this) && m_ConnectedTurrets.Contains(rhs));
    }

    public void ChangeWeapon(TurretWeapon weapon_prefab)
    {
        if (weapon_prefab)
        {
            int prevPowerDrain = UsedGridPower;

            Vector3 position = m_Weapon.transform.position;
            GameSession.FreeInstance(m_Weapon);
            m_Weapon = GameSession.AllocateInstance(weapon_prefab);
            m_Weapon.transform.position = position;
            m_Weapon.ResetFireCooldown();
            m_Weapon.SortOrder = m_EmissiveMaterials.SortOrder + 1;
            m_Weapon.SetPowered(IsTurretPowered);
            m_Weapon.gameObject.SetActive(HasRouteToCore);

            // Have to update the grid power, since we might overrun our power budget (or restore it if it's a power plant)
            // Check if we were previously on low power
            if (IsGridPowered() && (prevPowerDrain > GameSession.Instance.PowerLimit))
            {
                // Have to test for loops, as restoring power might activate them
                TestGridLoops();
            }
            else
            {
                // Just update the power state
                UpdateGridPower();
            }
        }
    }

    public void DisconnectAllWalls()
    {
        LoopTurretIDs.Clear();
        for (int i = ConnectedTurrets.Count-1; i >=0; --i)
        {
            ulong id_removed = CreateConnectionID(this, ConnectedTurrets[i]);
            LoopTurretIDs.Add(id_removed);
            BreakConnectionBetween(this, ConnectedTurrets[i]);
        }
        TurretWall.BreakLoopWalls(LoopTurretIDs);
        TestGridLoops();
    }

    public static void FormTurretConnection(TurretBehaviour from, TurretBehaviour to)
    {
        // Make sure we aren't connecting turret to itself, or to a turret that's already connected
        if ((from == to) || from.IsConnectedToTurret(to))
        {
            return;
        }

        bool wasFromPowered = from.IsTurretPowered;
        bool wasToPowered = to.IsTurretPowered;

        // Hook up the turrets
        from.m_ConnectedTurrets.Add(to);
        to.m_ConnectedTurrets.Add(from);
        SpawnWallBetween(from, to);

        // Check the changes to the power state
        if (IsGridPowered())
        {
            // Grid is going to be powered up, check if any loops have formed, handle accordingly
            if(wasFromPowered || wasToPowered)
            {
                // Only test for loops if we made a connection between powered turrets
                TestGridLoops();
            }
        }

        // Update the power state of the walls
        TurretWall.PropagatePower();
    }

    public static ulong CreateConnectionID(TurretBehaviour from, TurretBehaviour to)
    {
        uint from_id = (uint)from.GetInstanceID();
        uint to_id   = (uint)to.GetInstanceID();
        return from_id < to_id ? (from_id | ((ulong)to_id) << 32)
                : (to_id | ((ulong)from_id) << 32);
    }

    private static void SpawnWallBetween(TurretBehaviour from, TurretBehaviour to)
    {
        //Spawn a wall
        TurretWall wall_prefab = GameSession.Instance?.WallPrefab;
        if (wall_prefab)
        {
            TurretWall new_wall = GameSession.AllocateInstance(wall_prefab);
            new_wall.SetBeginAndEnd(from, to);
        }
    }

    private void FormCoreConnection()
    {
        GameSession active_session = GameSession.Instance;
        DefenseCore defense_core   = DefenseCore.ActiveDefenseCore;
        TurretWall wall_prefab     = active_session?.WallPrefab;
        if (IsTurretCore && defense_core && wall_prefab)
        {
            TurretWall new_wall = GameSession.AllocateInstance(wall_prefab);
            new_wall.ScaleToPoints(defense_core.transform.position, transform.position);
            new_wall.IsPowered = false; //TODO: Replace with something better
            new_wall.OverrideColourAndEmissive(Color.grey, Color.yellow);
        }
    }

    private static void ResetGridPowerState(bool drainBelowLimit = true)
    {
        foreach (TurretBehaviour currentTurret in TurretInstances)
        {
            currentTurret.m_nodeVisited = false;
            currentTurret.IsTurretPowered = drainBelowLimit && currentTurret.IsTurretCore; // Core turrets are always powered (except when LOW POWER)
        }
    }

    private static int GetUsedGridPower()
    {
        ResetGridPowerState();
        foreach (TurretBehaviour currentTurret in TurretInstances)
        {
            currentTurret.HasRouteToCore = false;
        }
        // Get the core turrets
        List<TurretBehaviour> coreTurretList = new List<TurretBehaviour>();
        foreach (TurretBehaviour currentTurret in TurretInstances)
        {
            if (currentTurret.IsTurretCore)
            {
                coreTurretList.Add(currentTurret);
                currentTurret.HasRouteToCore = true;
            }
        }

        List<TurretBehaviour> turretQueue = new List<TurretBehaviour>();
        foreach (TurretBehaviour coreTurret in coreTurretList)
        {
            if (coreTurret.m_nodeVisited)
            {
                continue;
            }
            turretQueue.Add(coreTurret);

            int currentIndex = 0;
            while (currentIndex < turretQueue.Count)
            {
                TurretBehaviour currentTurret = turretQueue[currentIndex];
                currentTurret.m_nodeVisited = true;
                currentTurret.HasRouteToCore = true;
                currentTurret.IsTurretPowered = true;

                foreach (TurretBehaviour currentConnection in currentTurret.m_ConnectedTurrets)
                {
                    if (!currentConnection.m_nodeVisited && !turretQueue.Contains(currentConnection))
                    {
                        turretQueue.Add(currentConnection);
                    }
                }
                ++currentIndex;
            }
        }

        int power_drain = 0;
        int battery_power = 0;

        //Sweep the turrets connected to the core and accumulate the power
        foreach (TurretBehaviour turret in TurretInstances)
        {
            if (turret.HasRouteToCore)
            {
                int turret_drain = turret.m_Weapon ? turret.m_Weapon.PowerUsage : GameSession.Instance.DefaultTurretDrain; // Use the default drain on an empty turret (this might be redundant? Can you have an active turret with no weapon?)

                //Core turrets cannot cost power, but they can provide it
                if (turret.IsTurretCore)
                {
                    turret_drain = Mathf.Min(turret_drain, 0);
                }

                if (turret_drain > 0)
                {
                    power_drain += turret_drain;
                }
                //Negative drain gives battery power
                else if (turret_drain < 0)
                {
                    battery_power -= turret_drain;
                }
            }
            else
            {
                turret.ApplyPowerState();
            }
        }

        UsedGridPower = power_drain;
        BatteryPower = battery_power;

        // Power limit can handle this many turrets, BUILDING ONLINE
        return UsedGridPower;
    }

    private static bool IsGridPowered()
    {
        int old_power = UsedGridPower;
        int old_battery = BatteryPower;
        bool result = IsGridPowered(GetUsedGridPower());
        if (old_power != UsedGridPower || old_battery != BatteryPower)
        {
            UsedGridPowerChanged?.Invoke(UsedGridPower);
        }
        return result;
    }

    private static bool IsGridPowered(int used)
    {
        int powerDrain = used;
        if (powerDrain > GameSession.Instance.PowerLimit)
        {
            // Too many turrets are connected, LOW POWER
            ResetGridPowerState(false);
            return false;
        }
        return true;
    }

    private static void UpdateGridPower()
    {
        int old_power = UsedGridPower;
        int old_battery = BatteryPower;

        IsGridPowered(GetUsedGridPower()); // The test also updates the power state
        TurretWall.PropagatePower(); // Update the power state of the walls

        if (old_power != UsedGridPower || old_battery != BatteryPower)
        {
            UsedGridPowerChanged?.Invoke(UsedGridPower);
        }
    }

    private static void TestGridLoops()
    {
        ResetGraphVisited();
        
        // Get the core turrets
        List<TurretBehaviour> coreTurretList = new List<TurretBehaviour>();
        foreach (TurretBehaviour currentTurret in TurretInstances)
        {
            if (currentTurret.IsTurretCore)
            {
                coreTurretList.Add(currentTurret);
            }
        }

        // Start a depth-first propagation from the core turrets. If we find a loop, gather its nodes from the DFS stack, asplode it,
        // then reset the process until we can verify that the turret graph is a tree
        List<DFSNode> DFSStack = new List<DFSNode>();
        for(int coreTurretIndex = 0; coreTurretIndex < coreTurretList.Count; ++coreTurretIndex)
        {
            // Start from a core turret, depth-first explore the graph to validate that there are no loops
            TurretBehaviour currentCoreTurret = coreTurretList[coreTurretIndex];
            if(currentCoreTurret.m_nodeVisited)
            {
                // Skip core, since we already visited it (e.g had a connection between 2 core turrets)
                continue;
            }

            DFSStack.Add(new DFSNode(currentCoreTurret, 0));
            while(DFSStack.Count > 0)
            {
                DFSNode stackTopNode = DFSStack[DFSStack.Count - 1];
                stackTopNode.m_turret.m_nodeVisited = true;
                if(stackTopNode.m_index >= stackTopNode.m_turret.m_ConnectedTurrets.Count)
                {
                    // Processed all the connections of this node, pop from the stack
                    DFSStack.PopBack();
                    continue;
                }

                TurretBehaviour currentConnection = stackTopNode.m_turret.m_ConnectedTurrets[stackTopNode.m_index];
                if(DFSStack.Count > 1)
                {
                    // Check if the current connection is the node just below us in the stack
                    DFSNode stackPrevNode = DFSStack[DFSStack.Count - 2];
                    if(currentConnection == stackPrevNode.m_turret)
                    {
                        // We can ignore this connection (we just came from there)
                        ++stackTopNode.m_index;
                        continue;
                    }
                }

                if(currentConnection.m_nodeVisited)
                {
                    // The connected node was already visited, so we found a loop, gather its nodes
                    int loopStartIndex = DFSStack.FindIndex(node => (node.m_turret == currentConnection));
                    List<TurretBehaviour> turretLoopList = new List<TurretBehaviour>();

                    for(int loopIndex = loopStartIndex; loopIndex < DFSStack.Count; ++loopIndex)
                    {
                        TurretBehaviour loopTurret = DFSStack[loopIndex].m_turret;
                        turretLoopList.Add(loopTurret);
                    }

                    // Handle turret loop logic (kill creeps inside loop, destroy the loop connections)
                    HandleTurretLoop(turretLoopList);

                    // Restart the process from the first core turret (necessary, as blowing up a loop can change the whole graph structure)
                    ResetGraphVisited();
                    DFSStack.Clear();
                    coreTurretIndex = -1;
                }
                else
                {
                    // Depth-first explore the connected node
                    DFSStack.Add(new DFSNode(currentConnection, 0));
                    ++stackTopNode.m_index; // Go to the next connection when the stack returns here
                }
            }
        }

        // Run the power propagation one more time, in case loops were blown up and thus left nodes without power
        // NOTE: blowing up connections can only reduce power drain, so there is no way we can be left with an unpowered grid by this point
        UpdateGridPower();
    }

    private static void HandleTurretLoop(List<TurretBehaviour> turretLoopList)
    {
        List<Vector2> loopVertices = new List<Vector2>();

        foreach(TurretBehaviour loopTurret in turretLoopList)
        {
            loopTurret.m_inLoop = true; // Set the flag so blowing up the connections becomes easier
            loopVertices.Add(loopTurret.transform.position2D());
        }

        // Gather all the creeps inside the loop
        List<Creep> encircledCreepList = new List<Creep>();
        foreach (Creep currentCreep in Creep.ActiveCreeps)
        {
            if (SpatialUtil.IsPointInsidePolygon(loopVertices, currentCreep.transform.position2D()))
            {
                encircledCreepList.Add(currentCreep);
            }
        }

        // Kill all the encircled creeps
        foreach (Creep currentCreep in encircledCreepList)
        {
            currentCreep.Damage(int.MaxValue);
        }
        TurretBehaviour.WallLoopCreated.Invoke(encircledCreepList.Count > 0);

        // Break the loop
        LoopTurretIDs.Clear();
        foreach (TurretBehaviour currentTurret in turretLoopList)
        {
            // Turret is in the loop, "break" all of its connections to other turrets that are in a loop
            foreach (TurretBehaviour connectedTurret in currentTurret.ConnectedTurrets)
            {
                if (connectedTurret.m_inLoop)
                {
                    // Remove self from the other loop members
                    connectedTurret.m_ConnectedTurrets.Remove(currentTurret);
                    ulong id_removed = CreateConnectionID(currentTurret, connectedTurret);
                    LoopTurretIDs.Add(id_removed);
                }
            }

            // Remove all neighbors that were in the loop and unset the flag
            currentTurret.m_ConnectedTurrets.RemoveAll(connectedTurret => (connectedTurret.m_inLoop));
            currentTurret.m_inLoop = false;
        }

        TurretWall.BreakLoopWalls(LoopTurretIDs);
        LoopTurretIDs.Clear();
    }

    private void ApplyPowerState()
    {
        if (m_Weapon)
        {
            m_Weapon.SetPowered(IsTurretPowered);
            m_Weapon.gameObject.SetActive(HasRouteToCore);
            m_Weapon.SortOrder = m_EmissiveMaterials.SortOrder + 1;
        }

        if (IsTurretPowered)
            m_EmissiveMaterials.RestoreEmissiveColour();
        else
            m_EmissiveMaterials.SetEmissiveColour(Color.black);
    }

#if UNITY_EDITOR
    public static void DrawTurretConnections()
    {
        ResetGraphVisited();
        foreach (TurretBehaviour currentTurret in TurretInstances)
        {
            currentTurret.m_nodeVisited = true;
            Vector2 currentPos = currentTurret.transform.position2D();
            foreach (TurretBehaviour connectedTurret in currentTurret.ConnectedTurrets)
            {
                if (connectedTurret.m_nodeVisited)
                {
                    continue;
                }
                Gizmos.DrawLine(currentPos, connectedTurret.transform.position2D());
            }
        }
    }
#endif
}
