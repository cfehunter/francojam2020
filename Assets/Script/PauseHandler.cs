﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class PauseHandler
{
    static bool m_Paused = false;
    static float m_InitialFixedDeltaTime = 0f;

    public static void PauseGame(bool paused)
    {
        if (paused == m_Paused)
            return;

        m_Paused = paused;

        if (m_InitialFixedDeltaTime == 0f)
        {
            m_InitialFixedDeltaTime = Time.fixedDeltaTime;
        }

        if (paused)
        {
            Time.timeScale = 0f;
            Time.fixedDeltaTime = 0f;
        }
        else
        {
            Time.timeScale = 1f;
            Time.fixedDeltaTime = m_InitialFixedDeltaTime;
        }
    }
}
