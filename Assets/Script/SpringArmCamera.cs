﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpringArmCamera : MonoBehaviour
{
    private Vector2         m_Velocity = Vector2.zero;
    public Vector2          DeadZone = Vector2.zero;
    public PlayerController Player;
    public float            SmoothTime = 0.3f;
    public float            MaxSpeed = 1.0f;

    private void Start()
    {
        if (!Player)
        {
            Player = FindObjectOfType<PlayerController>();
            if (!Player)
            {
                Debug.LogWarning("Camera has failed to find a player controller to follow");
            }
        }
    }

    private void Update()
    {
        if (Player)
        {
            Vector3 position = transform.position;
            Vector2 player_position = Player.transform.position2D();

            Vector2 delta = player_position - position.XY();

            bool move_x = Mathf.Abs(delta.x) > DeadZone.x * 0.5f;
            bool move_y = Mathf.Abs(delta.y) > DeadZone.y * 0.5f;
            if (move_x || move_y)
            {
                Vector2 desired_position = position.XY();
                if (move_x)
                    desired_position.x = player_position.x;

                if (move_y)
                    desired_position.y = player_position.y;

                Vector2 new_position_2D = Vector2.SmoothDamp(position, desired_position, ref m_Velocity, SmoothTime, MaxSpeed);
                transform.position = new Vector3(new_position_2D.x, new_position_2D.y, position.z);
            }
        }
    }

#if UNITY_EDITOR
    void OnDrawGizmosSelected()
    {
        Vector3 position = transform.position;
        Gizmos.color = Color.yellow;

        float half_x = DeadZone.x * 0.5f;
        float half_y = DeadZone.y * 0.5f;
        Gizmos.DrawLine(position.Translated(-half_x, -half_y), position.Translated(half_x, -half_y));
        Gizmos.DrawLine(position.Translated(-half_x, half_y), position.Translated(half_x, half_y));
        Gizmos.DrawLine(position.Translated(-half_x, half_y), position.Translated(-half_x, -half_y));
        Gizmos.DrawLine(position.Translated(half_x, half_y), position.Translated(half_x, -half_y));
    }
#endif
}
