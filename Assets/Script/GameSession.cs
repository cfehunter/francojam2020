﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.Serialization;

public enum GameState
{
    Won,
    Lost,
    Playing
}

[Serializable]
public class GameWaveDefinition
{
    public int    PowerBoost;
    public int    SpawnCountdown = 5;
    public string SplashText;
}

[RequireComponent(typeof(HexGrid), typeof(CreepSystem))]
public class GameSession : MonoBehaviour
{
    public static event Action<GameWaveDefinition, int> WaveStarted;
    public static event Action<GameWaveDefinition, int> WaveCountdownStarted;
    public static event Action<GameWaveDefinition, int> WaveCompleted;
    public static event Action<GameState>               StateChanged;

    public static GameSession           Instance      { get; private set; }
    public CreepSystem                  CreepSystem   { get; set; }
    public HexGrid                      HexGrid       { get; private set; }
    public GameState                    State         { get; private set; } = GameState.Playing;
    public int                          CurrentWave   { get; private set; } = 0;
    public float                        WaveCountdown { get; private set; } = 0f;
    private ObjectPool<Creep>           m_CreepPool;
    private ObjectPool<BulletBehaviour> m_BulletPool;
    private ObjectPool<TurretWall>      m_WallPool;
    private ObjectPool<TurretWeapon>    m_WeaponPool;
    private ObjectPool<ExplosionEffect> m_ExplosionPool;
    public DeferredCallbacks            DeferredCallbacks { get; private set; }

    [SerializeField] private TurretWall m_WallPrefab;
    public TurretWall WallPrefab => m_WallPrefab;

    [SerializeField] private int m_MaxWallHealth = 100;
    public int MaxWallHealth => m_MaxWallHealth;

    [SerializeField, FormerlySerializedAs("m_PowerLimit")] private int m_CorePowerLimit = 10;
    public int PowerLimit => m_CorePowerLimit + TurretBehaviour.BatteryPower;

    [SerializeField] private int m_defaultTurretDrain = 1;
    public int DefaultTurretDrain => m_defaultTurretDrain;

    [SerializeField] private List<GameWaveDefinition> m_Waves = new List<GameWaveDefinition>();
    public IReadOnlyList<GameWaveDefinition> Waves => m_Waves;

    public static Creep AllocateInstance(Creep prefab)
    {
        if (!Instance)
            return Instantiate(prefab);
        return Instance.m_CreepPool.Allocate(prefab);
    }

    public static BulletBehaviour AllocateInstance(BulletBehaviour prefab)
    {
        if (!Instance)
            return Instantiate(prefab);
        return Instance.m_BulletPool.Allocate(prefab);
    }

    public static TurretWall AllocateInstance(TurretWall prefab)
    {
        if (!Instance)
            return Instantiate(prefab);
        return Instance.m_WallPool.Allocate(prefab);
    }

    public static TurretWeapon AllocateInstance(TurretWeapon prefab)
    {
        if (!Instance)
            return Instantiate(prefab);
        return Instance.m_WeaponPool.Allocate(prefab);
    }

    public static ExplosionEffect AllocateInstance(ExplosionEffect prefab)
    {
        if (!Instance)
            return Instantiate(prefab);
        return Instance.m_ExplosionPool.Allocate(prefab);
    }

    public static void FreeInstance(Creep prefab)
    {
        if (!Instance)
            Destroy(prefab.gameObject);
        else
            Instance.m_CreepPool.Free(prefab);
    }

    public static void FreeInstance(BulletBehaviour prefab)
    {
        if (!Instance)
            Destroy(prefab.gameObject);
        else
            Instance.m_BulletPool.Free(prefab);
    }

    public static void FreeInstance(TurretWall prefab)
    {
        if (!Instance)
            Destroy(prefab.gameObject);
        else
            Instance.m_WallPool.Free(prefab);
    }

    public static void FreeInstance(TurretWeapon prefab)
    {
        if (!Instance)
            Destroy(prefab.gameObject);
        else
            Instance.m_WeaponPool.Free(prefab);
    }

    public static void FreeInstance(ExplosionEffect prefab)
    {
        if (!Instance)
            Destroy(prefab.gameObject);
        else
            Instance.m_ExplosionPool.Free(prefab);
    }

    public static void GotoGameLoss()
    {
        SceneManager.LoadScene(2, LoadSceneMode.Single);
    }

    public static void GotoGameWin()
    {
        SceneManager.LoadScene(1, LoadSceneMode.Single);
    }

    public static void NextLevel()
    {
        int next = SceneManager.GetActiveScene().buildIndex + 1;
        if (next >= SceneManager.sceneCountInBuildSettings)
        {
            GotoGameWin();
        }
        else
        {
            SceneManager.LoadScene(next, LoadSceneMode.Single);
        }
    }

    void Awake()
    {
        DeferredCallbacks = new DeferredCallbacks();
        m_CreepPool       = new ObjectPool<Creep>(20, "Creep Container", transform);
        m_BulletPool      = new ObjectPool<BulletBehaviour>(10, "Bullet Container", transform);
        m_WallPool        = new ObjectPool<TurretWall>(5, "Wall Container", transform);
        m_WeaponPool      = new ObjectPool<TurretWeapon>(5, "Weapon Container", transform);
        m_ExplosionPool   = new ObjectPool<ExplosionEffect>(20, "Explosion Container", transform);
        HexGrid           = GetComponent<HexGrid>();

        CreepSystem = GetComponent<CreepSystem>();
        DefenseCore.DefenseCoreDamaged += OnDefenseCoreDamaged;
        Creep.CreepDestroyed += OnCreepDestroyed;
        CutscenePlayer.OnCutsceneDialogChanged += OnCutsceneDialog;

        Instance = this;
    }

    void Start()
    {
        if (m_Waves.Count > 0)
        {
            WaveCountdown = m_Waves[0].SpawnCountdown;
            WaveCountdownStarted?.Invoke(m_Waves[0], 0);
        }
    }

    private void Update()
    {
#if DEBUG
        if (Input.GetKeyDown(KeyCode.N))
        {
            NextLevel();
        }
        if (Input.GetKeyDown(KeyCode.K))
        {
            foreach (CreepSpawner spawner in CreepSpawner.ActiveSpawners)
            {
                spawner.ClearPendingSpawns();
            }

            IReadOnlyList<Creep> active_creeps = Creep.ActiveCreeps;
            for (int i = active_creeps.Count -1; i >= 0; --i)
            {
                active_creeps[i].Damage(9999);
            }
        }
#endif
    }

    void FixedUpdate()
    {
        if (WaveCountdown > 0f && State == GameState.Playing)
        {
            WaveCountdown -= Time.fixedDeltaTime;
            if (WaveCountdown <= 0f)
            {
                WaveCountdown = 0f;
                StartWave();
            }
        }

        DeferredCallbacks.Update(Time.fixedDeltaTime);
    }

    void OnDestroy()
    {
        DefenseCore.DefenseCoreDamaged -= OnDefenseCoreDamaged;
        Creep.CreepDestroyed -= OnCreepDestroyed;
        CutscenePlayer.OnCutsceneDialogChanged -= OnCutsceneDialog;

        //Make sure to unpause
        PauseHandler.PauseGame(false);

        if (Instance == this)
            Instance = null;
    }

    private void GameLost()
    {
        Debug.Log("YOU LOSE! I BLAME CHRIS");
        State = GameState.Lost;
        StateChanged?.Invoke(State);

        DeferredCallbacks.AddCallback(() => GotoGameLoss(), 3f);
    }

    private void GameWon()
    {
        Debug.Log("YOUN WIN! RAPTOR JESUS IS SAD!");
        State = GameState.Won;
        StateChanged?.Invoke(State);

        DeferredCallbacks.AddCallback(() => NextLevel(), 3f);
    }

    private void EndWave()
    {
        if (CurrentWave < m_Waves.Count)
        {
            WaveCompleted?.Invoke(m_Waves[CurrentWave], CurrentWave);
        }
    }

    private void StartWave()
    {
        if (CurrentWave < m_Waves.Count)
        {
            WaveStarted?.Invoke(m_Waves[CurrentWave], CurrentWave);

            if (!CreepSystem.SpawnCreepsForWave(CurrentWave))
            {
                AdvanceWave();
            }
        }
    }

    private void AdvanceWave()
    {
        EndWave();
        ++CurrentWave;

        if (CurrentWave >= m_Waves.Count)
        {
            DeferredCallbacks.AddCallback(() => GameWon(), 3f); //Last second hack
        }
        else if (State == GameState.Playing)
        {
            WaveCountdown = m_Waves[CurrentWave].SpawnCountdown;
            WaveCountdownStarted?.Invoke(m_Waves[CurrentWave], CurrentWave);
        }
    }

    #region Event Handlers

    private void OnDefenseCoreDamaged(DefenseCore core)
    {
        if (core.Health <= 0)
        {
            GameLost();
        }
    }

    private void OnCreepDestroyed(Creep obj)
    {
        if (Creep.ActiveCreeps.Count == 0 && !CreepSpawner.AnySpawnsPending)
            AdvanceWave();
    }

    private void OnCutsceneDialog(DialogLine obj)
    {
        PauseHandler.PauseGame(obj != null);
    }

    #endregion
}
