﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IPooledObject
{
    int  PrefabID { get; set; }
}

public class ObjectPool<T> where T : MonoBehaviour, IPooledObject
{
    Dictionary<int, List<T>> m_PrefabMap = new Dictionary<int, List<T>>();
    int                      m_PoolGrowth;
    Transform                m_PoolContainer;

    public ObjectPool(int PoolGrowth, string container_name, Transform container_parent = null)
    {
        m_PoolGrowth = Mathf.Max(1, PoolGrowth);
        m_PoolContainer = new GameObject(container_name).transform;
        if (container_parent)
            m_PoolContainer.parent = container_parent;
    }

    public T Allocate(T prefab)
    {
        EnsurePrefabID(prefab);
        List<T> pool = GetPoolForPrefab(prefab);
        EnsurePoolCapacity(pool, prefab);

        T result = pool.PopBack();
        result.gameObject.SetActive(true);
        return result;
    }

    public void Free(T obj)
    {
        if (obj.PrefabID != 0)
        {
            obj.gameObject.SetActive(false);
            m_PrefabMap[obj.PrefabID].Add(obj);
        }
        else
        {
            Object.Destroy(obj.gameObject);
        }
    }

    private void EnsurePrefabID(T prefab)
    {
        if (prefab.PrefabID == 0)
        {
            prefab.PrefabID = prefab.GetInstanceID();
        }
    }

    private void EnsurePoolCapacity(List<T> pool, T prefab)
    {
        if (pool.Count == 0)
        {
            for (int i = 0; i < m_PoolGrowth; ++i)
            {
                T new_object = Object.Instantiate(prefab, m_PoolContainer);
                new_object.PrefabID = prefab.PrefabID;
                new_object.gameObject.SetActive(false);
                pool.Add(new_object);
            }
        }
    }

    private List<T> GetPoolForPrefab(T prefab)
    {
        int id = prefab.PrefabID;

        List<T> result;
        if (!m_PrefabMap.TryGetValue(id, out result))
        {
            result = new List<T>(m_PoolGrowth);
            m_PrefabMap.Add(id, result);
        }
        return result;
    }
}
