﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletBehaviour : MonoBehaviour, IPooledObject
{
    // Start is called before the first frame update
    public Creep                   Target { get; set; }
    private Vector2? m_targetLocation;
    [SerializeField] private float m_Speed = 2.0f;
    [SerializeField] private int   m_Damage = 1;

    public int PrefabID { get; set; }

    void Awake()
    {
        Creep.CreepDestroyed += OnCreepDestroyed;
    }


    void OnEnable()
    {
        m_targetLocation = null;
    }
    void OnDisable()
    {
        m_targetLocation = null;
    }

    void OnDestroy()
    {
        Creep.CreepDestroyed -= OnCreepDestroyed;
    }

    void OnCreepDestroyed(Creep destroyed)
    {
        if (Target == destroyed)
        {
            Target = null;
        }
    }
    // Update is called once per frame
    void FixedUpdate()
    {
        if (Target)
        {
            m_targetLocation = Target.transform.position2D();
        }
        else if (m_targetLocation == null && !Target)
        {
            GameSession.FreeInstance(this);
        }
        
        var move_vector = (m_targetLocation ?? Vector2.zero) - transform.position.XY();
        var move_direction = move_vector.normalized;
        var velocity_step = m_Speed * Time.fixedDeltaTime;
        transform.position += move_direction.ToVector3() * velocity_step;

        //Rotate to facing
        transform.rotation = Quaternion.FromToRotation(Vector3.right, move_direction);

        if (move_vector.sqrMagnitude <= velocity_step * velocity_step)
        {
            // Do damage
            Target?.Damage(m_Damage);
            GameSession.FreeInstance(this);
        }
    }
}
