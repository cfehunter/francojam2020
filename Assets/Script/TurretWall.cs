﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

[RequireComponent(typeof(EdgeCollider2D), typeof(SpriteRenderer), typeof(SpriteEmissiveMaterials)),RequireComponent(typeof(Animator))]
public partial class TurretWall : MonoBehaviour, IPooledObject
{
    private static readonly int NormalAnimStateID = Animator.StringToHash("Normal");
    private static readonly int ExplodeStateID = Animator.StringToHash("Explode");

    public static event Action<TurretWall> ReceiveDamage;
    public static event Action<TurretWall> LinkStarted;
    public static event Action<TurretWall> LinkCompleted;
    private static List<TurretWall>         s_ActiveWalls = new List<TurretWall>();
    public static IReadOnlyList<TurretWall> ActiveWalls => s_ActiveWalls;
    public static event Action<TurretWall>  WallDestroyed;

    private SpriteEmissiveMaterials         m_EmissiveMaterials;
    private SpriteRenderer                  m_Sprite;
    private Animator                        m_Animator;
    public EdgeCollider2D                   Collider { get; private set; }
    public int                              Health   { get; private set; } = 1;
    public int                              MaxHealth => GameSession.Instance.MaxWallHealth;
    public int                              PrefabID { get; set; }
    public ulong                            ConnectionID { get; set; } = 0UL;

    private TurretBehaviour m_BeginTurret;
    public TurretBehaviour BeginTurret
    {
        get => m_BeginTurret;
        set
        {
            m_BeginTurret = value;
            ConfigureForTurrets();
        }
    }

    private TurretBehaviour m_EndTurret;
    public TurretBehaviour EndTurret
    {
        get => m_EndTurret;
        set
        {
            m_EndTurret = value;
            ConfigureForTurrets();
        }
    }

    private bool m_Powered = true;
    public bool IsPowered
    {
        get => m_Powered;
        set
        {
            m_Powered = value;
            Collider.enabled = value;

            if (m_BeginTurret && m_EndTurret) //Core walls should be left alone
            {
                if (value)
                {
                    RestoreColourAndEmissive();
                }
                else
                {
                    OverrideColourAndEmissive(Color.red, Color.black);
                }
            }
        }
    }

    private void Awake()
    {
        m_Sprite = GetComponent<SpriteRenderer>();
        m_Animator = GetComponent<Animator>();
        Collider = GetComponent<EdgeCollider2D>();
        m_EmissiveMaterials = GetComponent<SpriteEmissiveMaterials>();
    }

    private void Start()
    {
        Health = MaxHealth;
    }

    private void OnEnable()
    {
        if (GameSession.Instance)
            Health = MaxHealth;

        m_Animator.Play(NormalAnimStateID);
        s_ActiveWalls.Add(this);
        ConfigureForTurrets();
        LinkStarted?.Invoke(this);
    }

    private void OnDisable() //This is also called on destruction
    {
        s_ActiveWalls.Remove(this);
        m_EndTurret = m_BeginTurret = null;
    }

    public static void PropagatePower()
    {
        foreach (TurretWall wall in ActiveWalls)
        {
            wall.IsPowered = wall.BeginTurret && wall.EndTurret
                && wall.BeginTurret.IsTurretPowered && wall.EndTurret.IsTurretPowered;
        }
    }

    public static void BreakLoopWalls(IReadOnlyList<ulong> broken_wall_ids)
    {
        for (int i = ActiveWalls.Count - 1; i >= 0; --i)
        {
            TurretWall wall = ActiveWalls[i];
            if (wall.ConnectionID != 0UL)
            {
                if (broken_wall_ids.Contains(wall.ConnectionID))
                {
                    wall.DestroyWall();
                }
            }
        }
    }

    public bool Damage(int damage)
    {

        Health = Mathf.Max(0, Health - damage);
        ReceiveDamage?.Invoke(this);
        if (Health == 0)
        {
            if (m_BeginTurret && m_EndTurret)
                TurretBehaviour.BreakConnectionBetween(m_BeginTurret, m_EndTurret);
            
            DestroyWall();
            return true;
        }
        return false;
    }

    public void RestoreColourAndEmissive()
    {
        m_Sprite.color = Color.white;
        m_EmissiveMaterials.RestoreEmissiveColour();
    }

    public void OverrideColourAndEmissive(Color colour, Color emissive_colour)
    {
        m_Sprite.color = colour;
        m_EmissiveMaterials.SetEmissiveColour(emissive_colour);
    }

    public void ScaleToPoints(Vector2 begin, Vector2 end)
    {
        if (begin.Approximately(end))
            return;

        SpriteRenderer sprite = m_Sprite ? m_Sprite : GetComponent<SpriteRenderer>();
        EdgeCollider2D collider = Collider ? Collider : GetComponent<EdgeCollider2D>();

        Vector2 to_end = end - begin;
        float length = to_end.magnitude;

        transform.position = (begin + end) * 0.5f;
        transform.rotation = Quaternion.FromToRotation(Vector2.right, to_end / length);
        sprite.size = new Vector2(Mathf.Abs(length), sprite.size.y);

        float half_length = length * 0.5f;
        collider.points = new Vector2[2] { new Vector2(-half_length, 0f), new Vector2(half_length, 0f) };
    }

    public void SetBeginAndEnd(TurretBehaviour begin, TurretBehaviour end)
    {
        m_BeginTurret = begin;
        m_EndTurret = end;

        LinkCompleted?.Invoke(this);

        ConfigureForTurrets();
    }

    private void ConfigureForTurrets()
    {
        if (!m_BeginTurret || !m_EndTurret)
        {
            ConnectionID = 0L;
            return;
        }

        ConnectionID = TurretBehaviour.CreateConnectionID(m_BeginTurret, m_EndTurret);
        IsPowered = m_BeginTurret.IsTurretPowered || m_EndTurret.IsTurretPowered;
        ScaleToPoints(m_BeginTurret.transform.position2D(), m_EndTurret.transform.position);
    }

    private void DestroyWall()
    {
        //Lock the colour for the duration of the destruction anim
        m_EmissiveMaterials.Locked = true;
        m_Animator.Play(ExplodeStateID);
        WallDestroyed?.Invoke(this);
    }

    private void DestroyWallImmediate()
    {
        GameSession.FreeInstance(this);
    }
}

#if UNITY_EDITOR
//Editor junk
public partial class TurretWall
{
    [HideInInspector] public Vector2 m_EditorBegin;
    [HideInInspector] public Vector2 m_EditorEnd;
}
#endif