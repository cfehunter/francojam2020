﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum WeaponTargettingMode
{
    ClosestToSelf,
    ClosestToCore,
    BiggestEnemy,
    WeakestEnemy
}

public class TurretWeapon : MonoBehaviour, IPooledObject
{
    //Serialised
    [SerializeField] private WeaponDefinition m_Definition = null;
    public static event Action<TurretWeapon> WeaponFired;
    //Runtime
    private Transform        m_Muzzle = null;
    private float            m_ReloadTimer = 1.0f;
    private Creep            m_LastTarget = null;

    public float            ReloadTimer => m_ReloadTimer;
    public WeaponDefinition Definition => m_Definition;

    private SpriteRenderer m_spriteRenderer = null;
    private SpriteEmissiveMaterials m_EmissiveMaterials = null;

    public int SortOrder { get => m_spriteRenderer.sortingOrder; set => m_spriteRenderer.sortingOrder = value; }
    public int PrefabID { get; set; }
    public int PowerUsage { get => Definition.PowerUsage; }

    private bool IsPowered;
    private Color m_StartColour = Color.white;
    public void SetPowered(bool isPowered)
    {
        IsPowered = isPowered;
        m_spriteRenderer.color = isPowered ? m_StartColour : Color.grey;

        if (m_EmissiveMaterials)
        {
            if (isPowered)
                m_EmissiveMaterials.RestoreEmissiveColour();
            else
                m_EmissiveMaterials.SetEmissiveColour(Color.black);
        }
    }

    private void Awake()
    {
        Debug.Assert(m_Definition);
        m_Muzzle = transform.Find("muzzle") ?? transform;
        m_spriteRenderer = GetComponentInChildren<SpriteRenderer>();
        m_EmissiveMaterials = GetComponentInChildren<SpriteEmissiveMaterials>();
        m_spriteRenderer.sprite = Definition.WeaponSpriteFrames[0];
        m_StartColour = m_spriteRenderer.color;
    }

    private void OnEnable()
    {
        m_ReloadTimer = 0f;
    }

    private void FixedUpdate()
    {
        if( IsPowered == false )
        {
            return;
        }
        m_LastTarget = null;
        IReadOnlyList<Creep> active_creeps = Creep.ActiveCreeps;
        Creep chosen_target = null;
        float smallest_value = float.MaxValue;
        float biggest_value = float.MinValue;
        float sqr_max_range = m_Definition.SqrMaxRange;

        //Guard against using core targetting if there is no core
        var coreObject = DefenseCore.ActiveDefenseCore;

        foreach (Creep creep in active_creeps)
        {
            Vector2 creep_pos  = creep.transform.position2D();
            Vector2 turret_pos = transform.position2D();
            bool isInRange = ((creep_pos - turret_pos).sqrMagnitude) <= sqr_max_range;
            if (!isInRange)
                continue;

            switch (m_Definition.TargettingMode)
            {
                case WeaponTargettingMode.ClosestToSelf:
                {
                    var nextDistance = (creep.transform.position2D() - transform.position2D()).sqrMagnitude;
                    if (nextDistance < smallest_value)
                    {
                        chosen_target = creep;
                        smallest_value = nextDistance;
                    }
                }
                break;

                case WeaponTargettingMode.ClosestToCore:
                {
                    var nextDistance = (creep.transform.position2D() - coreObject.transform.position2D()).sqrMagnitude;
                    if (nextDistance < smallest_value)
                    {
                        chosen_target = creep;
                        smallest_value = nextDistance;
                    }
                }
                break;

                case WeaponTargettingMode.BiggestEnemy:
                {
                    var nextHealth = creep.MaxHitPoints;
                    if (nextHealth > biggest_value)
                    {
                        chosen_target = creep;
                        biggest_value = nextHealth;
                    }
                }
                break;

                case WeaponTargettingMode.WeakestEnemy:
                {
                    var nextHealth = creep.HitPoints;
                    if (nextHealth < smallest_value)
                    {
                        chosen_target = creep;
                        smallest_value = nextHealth;
                    }
                }
                break;
            }
        }

        m_LastTarget = chosen_target;
        m_ReloadTimer -= Time.fixedDeltaTime;
        if (chosen_target != null)
        {
            var to_target = chosen_target.transform.position2D() - transform.position2D();
            to_target.Normalize();

            var angleRange = (Mathf.PI * 2);
            var anglePerFrame = angleRange / Definition.WeaponSpriteFrames.Length;
            var angle = ((Mathf.Atan2(-to_target.y, -to_target.x) + Mathf.PI) + anglePerFrame/2);
            if (angle < 0.0f) angle += angleRange;
            if (angle >= angleRange) angle -= angleRange;
            m_spriteRenderer.sprite = Definition.WeaponSpriteFrames[Mathf.FloorToInt((angle / angleRange) * Definition.WeaponSpriteFrames.Length )];

            FireBulletAtTarget(chosen_target);
        }
    }

    void FireBulletAtTarget(Creep target)
    {
        if (m_ReloadTimer <= 0.0f && m_Definition.BulletPrefab)
        {
            var bullet = GameSession.AllocateInstance(m_Definition.BulletPrefab);
            bullet.transform.SetPositionAndRotation(transform.position2D(), Quaternion.identity);
            bullet.Target = target;
            bullet.transform.position = m_Muzzle.position2D();
            WeaponFired?.Invoke(this);
            ResetFireCooldown();
        }
    }

    public void ResetFireCooldown()
    {
        m_ReloadTimer = m_Definition.ReloadTime;
    }

#if UNITY_EDITOR
    private void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.red;
        var child = transform.Find("muzzle");
        var _fireFromPos = child?.position2D() ?? transform.position2D();
        if (m_LastTarget != null) Gizmos.DrawLine(_fireFromPos, m_LastTarget.transform.position2D());
        Gizmos.DrawWireSphere(transform.position, m_Definition.MaxRange);
        if (m_LastTarget != null) Gizmos.DrawWireSphere(m_LastTarget.transform.position2D(), 0.2f);
    }
#endif
}
