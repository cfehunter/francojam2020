﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Franco Jam/Weapon Definition")]
public class WeaponDefinition : ScriptableObject
{
    [SerializeField] private AudioClip _clip;
    [SerializeField] private float m_ReloadTime = 1f;
    [SerializeField] private float m_MaxRange = 5f;
    [SerializeField] private int m_powerUsage = 1;
    [SerializeField] private BulletBehaviour m_BulletPrefab;
    [SerializeField] private WeaponTargettingMode m_TargettingMode = WeaponTargettingMode.ClosestToSelf;
    [SerializeField] private Sprite[] m_weaponSpriteFrames;
    [SerializeField] private SFXSystem.SoundId _soundId;

    public float ReloadTime => m_ReloadTime;
    public float MaxRange => m_MaxRange;
    public float SqrMaxRange => m_MaxRange * m_MaxRange;
    public int PowerUsage => m_powerUsage;
    public BulletBehaviour BulletPrefab => m_BulletPrefab;
    public WeaponTargettingMode TargettingMode => m_TargettingMode;
    public Sprite[] WeaponSpriteFrames => m_weaponSpriteFrames;
    public SFXSystem.SoundId SoundId => _soundId;
}
