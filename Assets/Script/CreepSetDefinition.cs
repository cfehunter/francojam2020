﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[Serializable]
public class WeightedCreep
{
    public Creep Creep;
    public float Weight = 1.0f;
}

[CreateAssetMenu(menuName = "Franco Jam/Creep Set")]
public class CreepSetDefinition : ScriptableObject
{
    [HideInInspector]
    public List<WeightedCreep> Creeps = new List<WeightedCreep>();
}
