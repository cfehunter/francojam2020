﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Canvas), typeof(Image))]
public class WallHealthBar : MonoBehaviour
{
    [SerializeField] Gradient m_HealthColour;
    Canvas                    m_Canvas;
    TurretWall                m_Wall;
    Image                     m_HealthFill;
    int                       m_DisplayHealth = int.MinValue;

    private void Awake()
    {
        m_Canvas = GetComponent<Canvas>();

        m_Wall = GetComponentInParent<TurretWall>();
        Debug.Assert(m_Wall);

        m_HealthFill = GetComponent<Image>();
    }

    private void Start()
    {
        transform.rotation = Quaternion.identity;
    }

    private void OnEnable()
    {
        transform.rotation = Quaternion.identity;
        SetRenderEnabled(false);
    }

    private void FixedUpdate()
    {
        int current_health = m_Wall.Health;
        if (current_health != m_DisplayHealth)
        {
            //Toggle show/hide if we are now, or were, full health
            int max_health = m_Wall.MaxHealth;
            bool was_max_health = max_health == m_DisplayHealth;
            if (was_max_health || current_health == max_health)
            {
                SetRenderEnabled(was_max_health);
            }

            m_DisplayHealth = current_health;
            float normalised_health = m_DisplayHealth / (float)max_health;
            m_HealthFill.fillAmount = normalised_health;
            m_HealthFill.color = m_HealthColour.Evaluate(normalised_health);
        }
    }

    private void SetRenderEnabled(bool enabled)
    {
        m_Canvas.enabled = enabled;
    }
}
