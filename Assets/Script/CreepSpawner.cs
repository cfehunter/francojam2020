﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Serialization;

using Random = UnityEngine.Random;

[Serializable]
public class CreepSpawnerWave
{
    public int                Budget = 10;
    public float              StartDelay = 0.0f;

    [FormerlySerializedAs("PerCreepDelay")]
    public float              PerCreepDelayMin = 0.1f;
    public float              PerCreepDelayMax = 0.1f;
    public CreepSetDefinition CreepSet;
}

public class CreepSpawner : MonoBehaviour
{
    private static List<CreepSpawner> s_ActiveSpawners = new List<CreepSpawner>();
    public static IReadOnlyList<CreepSpawner> ActiveSpawners => s_ActiveSpawners;

    public static bool AnySpawnsPending => ActiveSpawners.Any(spawner => spawner.SpawnsPending);
    public static int TotalSpawnsPending => ActiveSpawners.Aggregate(0, (t, s) => t + s.m_PendingSpawns.Count);

    Vector2Int                                 m_HexPosition;
    List<Vector2Int>                           m_HexPath = new List<Vector2Int>();
    List<Vector2>                              m_Path = new List<Vector2>();
    [SerializeField] Vector2Int                m_TargetHex;
    List<Creep>                                m_PendingSpawns = new List<Creep>();
    public bool                                SpawnsPending => m_PendingSpawns.Count > 0;
    [SerializeField] List<CreepSpawnerWave>    m_Waves = new List<CreepSpawnerWave>();
    public List<CreepSpawnerWave>              Waves => m_Waves;
    float m_SpawnDelayMin = 0f;
    float m_SpawnDelayMax = 0f;
    float m_PendingSpawnTimer = 0f;

    private void Awake()
    {
        transform.position = transform.position2D();
    }

    private void Start()
    {
        GameSession active_game = GameSession.Instance;
        if (active_game)
        {
            m_TargetHex = active_game.HexGrid.GetCellPosition(DefenseCore.ActiveDefenseCore.transform.position);
        }
        else
        {
            Debug.LogWarning("Game Session is not Present. Game mode will not function");
        }

        UpdateHexPosition();
        RebuildPaths();

        s_ActiveSpawners.Add(this);
    }

    private void FixedUpdate()
    {
        if (SpawnsPending)
        {
            m_PendingSpawnTimer -= Time.fixedDeltaTime;
            if (m_PendingSpawnTimer <= 0f)
            {
                m_PendingSpawnTimer = RollSpawnTime();
                PopPendingSpawn();
            }
        }
    }

    private void OnDestroy()
    {
        s_ActiveSpawners.Remove(this);
    }

    public void UpdateHexPosition()
    {
        HexGrid hex_grid = GameSession.Instance?.HexGrid;
        if (hex_grid)
            m_HexPosition = hex_grid.GetCellPosition(transform.position);
    }

    public void RebuildPaths()
    {
        m_HexPath.Clear();

        HexGrid hex_grid = GameSession.Instance?.HexGrid;
        if (hex_grid && hex_grid.FindPath(m_HexPosition, m_TargetHex, m_HexPath))
        {
            m_Path.Clear();
            foreach (Vector2Int hex_position in m_HexPath)
            {
                m_Path.Add(hex_grid.GetWorldPosition(hex_position));
            }
        }
    }

    public int SpawnWave(int wave)
    {
        if (wave >= 0 && wave < m_Waves.Count)
        {
            RebuildPaths();
            CreepSpawnerWave spawner_wave = m_Waves[wave];
            if (spawner_wave != null)
            {
                return QueueSpawnsForWave(spawner_wave);
            }
        }
        return 0;
    }

    public void ClearPendingSpawns()
    {
        m_PendingSpawns.Clear();
    }

    private int QueueSpawnsForWave(CreepSpawnerWave wave)
    {
        if (wave.CreepSet == null || wave.Budget == 0)
            return 0;

        //Initialise the delay timers. First spawn is the wave delay
        m_SpawnDelayMin = wave.PerCreepDelayMin;
        m_SpawnDelayMax = wave.PerCreepDelayMax;
        m_PendingSpawnTimer = wave.StartDelay;

        List<WeightedCreep> creeps = new List<WeightedCreep>(wave.CreepSet.Creeps);
        int budget_remaining = wave.Budget;

        int amount_spawned = 0;
        while (budget_remaining > 0)
        {
            creeps.RemoveAll(x => x.Creep.Definition.SpawnCost > budget_remaining);
            if (creeps.Count <= 0)
                break;

            WeightedCreep selected = RandomUtility.WeightedRandomInterval(creeps, c => c.Weight);
            budget_remaining -= selected.Creep.Definition.SpawnCost;
            m_PendingSpawns.Add(selected.Creep);
            ++amount_spawned;
        }
        return amount_spawned;
    }

    private void PopPendingSpawn()
    {
        if (m_PendingSpawns.Count > 0)
        {
            Creep new_spawn = GameSession.AllocateInstance(m_PendingSpawns.PopBack());
            new_spawn.transform.SetPositionAndRotation(transform.position, Quaternion.identity);
            new_spawn.Path = m_Path;
        }
    }

    private float RollSpawnTime()
    {
        return Mathf.Max(0f, Random.Range(m_SpawnDelayMin, Mathf.Max(m_SpawnDelayMin, m_SpawnDelayMax)));
    }

#if UNITY_EDITOR
    private void OnDrawGizmosSelected()
    {
        HexGrid hex_grid = GameSession.Instance?.HexGrid;
        if (hex_grid && m_HexPosition != m_TargetHex)
        {
            Gizmos.color = Color.red;
            Gizmos.DrawSphere(hex_grid.GetWorldPosition(m_HexPosition), 0.5f);

            Gizmos.color = Color.yellow;
            foreach (Vector2 path_point in m_Path)
            {
                Gizmos.DrawCube(path_point, Vector3.one * 0.2f);
            }

            Gizmos.color = Color.green;
            Gizmos.DrawSphere(hex_grid.GetWorldPosition(m_TargetHex), 0.5f);
        }
    }
#endif
}
