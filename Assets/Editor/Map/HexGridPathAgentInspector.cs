﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(HexGridPathAgent))]
public class HexPathAgentInspector : Editor
{
    HexGridPathAgent m_target;

    void OnEnable()
    {
        m_target = (HexGridPathAgent)target;
    }

    public override void OnInspectorGUI()
    {
        DrawDefaultInspector();
        GUILayout.BeginHorizontal();
        if (GUILayout.Button("Rebuild Path"))
        {
            m_target.RebuildPath();
        }

        if (GUILayout.Button("Update Position"))
        {
            m_target.m_Position = m_target.m_HexGrid.GetCellPosition(m_target.transform.position);
            m_target.RebuildPath();
        }
        GUILayout.EndHorizontal();
    }
}
