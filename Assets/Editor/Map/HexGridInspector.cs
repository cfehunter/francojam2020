﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(HexGrid))]
public class HexGridInspector : Editor
{
    HexGrid m_target_grid;

    void OnEnable()
    {
        m_target_grid = (HexGrid)target;
    }

    public override void OnInspectorGUI()
    {
        //Debug.Log($"Grid Size: {m_target_grid.GetComponent<UnityEngine.Tilemaps.Tilemap>().cellBounds}");
        DrawDefaultInspector();

        GUILayout.BeginHorizontal();
        if (GUILayout.Button("Bake"))
        {
            Undo.RecordObject(target, "Bake Hexes");
            m_target_grid.BakeImpassable();
        }
        if (GUILayout.Button("Clear"))
        {
            if (EditorUtility.DisplayDialog("Confirm Clear", "Are you sure you want to clear the hex map?", "Yes", "No"))
            {
                Undo.RecordObject(target, "Clear Hexes");
                m_target_grid.ClearHexes();
            }
            GUIUtility.ExitGUI();
        }
        GUILayout.EndHorizontal();
    }
}
