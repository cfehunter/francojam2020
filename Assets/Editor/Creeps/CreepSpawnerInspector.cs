﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using System.Text.RegularExpressions;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(CreepSpawner))]
public class CreepSpawnerInspector : Editor
{
    CreepSpawner m_target_spawner;
    int          m_wave_to_spawn = 0;

    void OnEnable()
    {
        m_target_spawner = (CreepSpawner)target;
    }

    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();
        //DrawDefaultInspector();

        if (GUILayout.Button("Rebuild Paths"))
        {
            Undo.RecordObject(m_target_spawner, "Rebuild Spawner Paths");
            m_target_spawner.UpdateHexPosition();
            m_target_spawner.RebuildPaths();
        }

        GUILayout.BeginHorizontal();
        if (GUILayout.Button("Copy"))
        {
            CopyToClipboard();
        }
        if (GUILayout.Button("Paste"))
        {
            PasteFromClipboard();
        }
        GUILayout.EndHorizontal();

        if (Application.isPlaying)
        {
            GUILayout.BeginHorizontal();
            m_wave_to_spawn = Mathf.Max(0, EditorGUILayout.IntField("Wave", m_wave_to_spawn));
            if (GUILayout.Button("Spawn"))
            {
                m_target_spawner.SpawnWave(m_wave_to_spawn);
            }
            GUILayout.EndHorizontal();
        }

        serializedObject.ApplyModifiedProperties();
    }

    private void CopyToClipboard()
    {
        StringBuilder sb = new StringBuilder();
        sb.AppendLine("Budget\tStart Delay\tPer-Creep Delay Min\tPer-Creep Delay Max\tCreep Set");
        foreach (CreepSpawnerWave wave in m_target_spawner.Waves)
        {
            sb.AppendLine($"{wave.Budget:0}\t{wave.StartDelay:0.0}\t{wave.PerCreepDelayMin:0.0}\t{wave.PerCreepDelayMax:0.0}\t{AssetDatabase.GetAssetPath(wave.CreepSet)}");
        }
        GUIUtility.systemCopyBuffer = sb.ToString();
    }

    private void PasteFromClipboard()
    {
        string content = GUIUtility.systemCopyBuffer;
        Regex wave_regex = new Regex(@"([0-9]+)\t([0-9\.]+)\t([0-9\.]+)\t([0-9\.]+)\t(.+)");
        string[] lines = content.Split(new char[] { '\r', '\n' }, StringSplitOptions.RemoveEmptyEntries);

        List<CreepSpawnerWave> parsed_waves = new List<CreepSpawnerWave>();
        foreach (string line in lines)
        {
            Match match = wave_regex.Match(line);
            if (match.Success)
            {
                CreepSpawnerWave new_wave = new CreepSpawnerWave();
                new_wave.Budget = int.Parse(match.Groups[1].Value);
                new_wave.StartDelay = float.Parse(match.Groups[2].Value);
                new_wave.PerCreepDelayMin = float.Parse(match.Groups[3].Value);
                new_wave.PerCreepDelayMax = float.Parse(match.Groups[4].Value);
                new_wave.CreepSet = AssetDatabase.LoadAssetAtPath<CreepSetDefinition>(match.Groups[5].Value);

                if (new_wave.CreepSet)
                {
                    parsed_waves.Add(new_wave);
                }
                else
                {
                    Debug.LogError($"{match.Captures[4].Value} is not a valid creep set path");
                }
            }
        }


        if (parsed_waves.Count == 0)
        {
            EditorUtility.DisplayDialog("Invalid Paste", "No records found in clipboard data", "Okay");
        }
        else if (EditorUtility.DisplayDialog("Confirm Paste", $"Found {parsed_waves.Count} valid entries.\nReplacing {m_target_spawner.Waves.Count} existing entries.\nDo you want to continue?", "Yes", "No"))
        {
            m_target_spawner.Waves.Clear();
            m_target_spawner.Waves.AddRange(parsed_waves);
            EditorUtility.SetDirty(m_target_spawner);
        }
    }
}
