﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(TurretWall))]
public class TurretWallInspector : Editor
{
    TurretWall m_TargetWall;

    void OnEnable()
    {
        m_TargetWall = (TurretWall)target;
    }

    public override void OnInspectorGUI()
    {
        DrawDefaultInspector();

        EditorGUILayout.BeginHorizontal();
        float original_label_width = EditorGUIUtility.labelWidth;
        EditorGUIUtility.labelWidth = 60f;
        m_TargetWall.m_EditorBegin = EditorGUILayout.Vector2Field("Begin", m_TargetWall.m_EditorBegin);
        m_TargetWall.m_EditorEnd = EditorGUILayout.Vector2Field("End", m_TargetWall.m_EditorEnd);
        EditorGUIUtility.labelWidth = original_label_width;
        EditorGUILayout.EndHorizontal();

        if (GUILayout.Button("Apply"))
        {
            m_TargetWall.ScaleToPoints(m_TargetWall.m_EditorBegin, m_TargetWall.m_EditorEnd);
        }
    }
}
