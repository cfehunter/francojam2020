﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(CreepSetDefinition))]
public class CreepSetDefinitionInspector : Editor
{
    CreepSetDefinition       m_TargetSet;

    void OnEnable()
    {
        m_TargetSet = (CreepSetDefinition)target;
    }

    public override void OnInspectorGUI()
    {
        bool dirty = false;
        DrawDefaultInspector();

        List<WeightedCreep> creeps = m_TargetSet.Creeps;
        EditorGUILayout.BeginVertical();
        dirty |= DoListResize("Creep Types", creeps);

        float original_label_width = EditorGUIUtility.labelWidth;
        EditorGUIUtility.labelWidth = 60f;
        foreach (WeightedCreep current in creeps)
        {
            EditorGUILayout.BeginHorizontal();
            Creep new_creep  = (Creep)EditorGUILayout.ObjectField("Prefab", current.Creep, typeof(Creep), false);
            float new_weight = Mathf.Max(0f, EditorGUILayout.FloatField("Weight", current.Weight));

            if (new_creep != current.Creep || !Mathf.Approximately(new_weight, current.Weight))
            {
                current.Creep  = new_creep;
                current.Weight = new_weight;
                dirty = true;
            }
            EditorGUILayout.EndHorizontal();
        }
        EditorGUIUtility.labelWidth = original_label_width;
        EditorGUILayout.EndVertical();

        if (dirty)
        {
            EditorUtility.SetDirty(m_TargetSet);
        }
    }

    private bool DoListResize<T>(string label, List<T> list)
    {
        EditorGUILayout.BeginHorizontal();
        int new_size = Mathf.Max(0, EditorGUILayout.DelayedIntField(label, list.Count));
        if (new_size != list.Count)
        {
            list.Resize(new_size);
            GUIUtility.ExitGUI();
            return true;
        }
        EditorGUILayout.EndHorizontal();
        return false;
    }
}
