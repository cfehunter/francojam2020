﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(TurretBehaviour))]
public class TurretInspector : Editor
{
    TurretBehaviour m_TargetTurret;
    TurretWeapon    m_ReplacementWeapon;

    void OnEnable()
    {
        m_TargetTurret = (TurretBehaviour)target;
    }

    public override void OnInspectorGUI()
    {
        DrawDefaultInspector();

        EditorGUI.BeginDisabledGroup(!EditorApplication.isPlaying);
        m_ReplacementWeapon = (TurretWeapon)EditorGUILayout.ObjectField("Editor Weapon", m_ReplacementWeapon, typeof(TurretWeapon), false);
        if (GUILayout.Button("Change Weapon") && m_ReplacementWeapon)
        {
            m_TargetTurret.ChangeWeapon(m_ReplacementWeapon);
        }
        EditorGUI.EndDisabledGroup();
    }
}
