﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

class ComputeKernel
{
    private static readonly int TargetID = Shader.PropertyToID("Target");
    private static readonly int SourceID = Shader.PropertyToID("Source");

    private ComputeShader m_Shader;
    private int           m_ID;
    private int           m_XThreads;
    private int           m_YThreads;
    private Texture       m_LastSource;
    private Texture       m_LastTarget;

    public ComputeKernel(ComputeShader shader, string name)
    {
        m_Shader = shader;
        m_ID = shader.FindKernel(name);

        //Query rquires uints, dispatch requires ints. Weird interface quirk...
        uint uthreads_x;
        uint uthreads_y;
        shader.GetKernelThreadGroupSizes(m_ID, out uthreads_x, out uthreads_y, out uint _);
        m_XThreads = (int)uthreads_x;
        m_YThreads = (int)uthreads_y;
    }

    public void Dispatch(int width, int height)
    {
        m_Shader.Dispatch(m_ID, ThreadDivide(width, m_XThreads), ThreadDivide(height, m_YThreads), 1);
    }

    public void SetSourceTexture(Texture texture)
    {
        if (m_LastSource != texture)
        {
            m_LastSource = texture;
            m_Shader.SetTexture(m_ID, SourceID, texture);
        }
    }

    public void SetTargetTexture(Texture texture)
    {
        if (m_LastTarget != texture)
        {
            m_LastTarget = texture;
            m_Shader.SetTexture(m_ID, TargetID, texture);
        }
    }

    private int ThreadDivide(int n, int threads)
    {
        return (n + threads - 1) / threads;
    }
}

public class EllipsePainter
{
    private static readonly int OffsetID = Shader.PropertyToID("Offset");
    private static readonly int EllipseSpecID = Shader.PropertyToID("EllipseSpec");
    private static readonly int PaintColourID = Shader.PropertyToID("PaintColour");
    private static readonly int FeatherID = Shader.PropertyToID("Feather");

    private Color         m_LastColour = Color.clear;
    private float         m_LastFeather = float.NaN;
    private ComputeShader m_PaintShader;
    private ComputeKernel m_PaintKernel;
    private ComputeKernel m_ClearKernel;
    private ComputeKernel m_AddKernel;
    private ComputeKernel m_SubtractKernel;
    private ComputeKernel m_CopyKernel;

    public EllipsePainter()
    {
        m_PaintShader = AssetDatabase.LoadAssetAtPath<ComputeShader>("Assets/Shader/Paint.compute");
        Debug.Assert(m_PaintShader);

        m_PaintKernel = new ComputeKernel(m_PaintShader, "CSPaint");
        m_ClearKernel = new ComputeKernel(m_PaintShader, "CSClear");
        m_AddKernel = new ComputeKernel(m_PaintShader, "CSAdd");
        m_SubtractKernel = new ComputeKernel(m_PaintShader, "CSSubtract");
        m_CopyKernel = new ComputeKernel(m_PaintShader, "CSCopy");
    }

    public void DrawEllipse(RenderTexture target, Vector2 position, float width, float height, Color colour, float feather)
    {
        Vector2 offset = position - new Vector2(width, height);
        m_PaintShader.SetVector(OffsetID, offset);
        
        Vector2 transformed_ellipse_pos = position - offset;
        m_PaintShader.SetVector(EllipseSpecID, new Vector4(transformed_ellipse_pos.x, transformed_ellipse_pos.y, width, height));

        if (colour != m_LastColour)
        {
            m_LastColour = colour;
            m_PaintShader.SetVector(PaintColourID, colour);
        }

        if (feather != m_LastFeather)
        {
            m_LastFeather = feather;
            m_PaintShader.SetFloat(FeatherID, feather);
        }

        m_PaintKernel.SetTargetTexture(target);
        m_PaintKernel.Dispatch(Mathf.CeilToInt(width * 2), Mathf.CeilToInt(height * 2));
    }

    public void AddTexture(Texture source, RenderTexture target)
    {
        m_AddKernel.SetSourceTexture(source);
        m_AddKernel.SetTargetTexture(target);

        m_AddKernel.Dispatch(target.width, target.height);
    }

    public void SubtractTexture(Texture source, RenderTexture target)
    {
        m_SubtractKernel.SetSourceTexture(source);
        m_SubtractKernel.SetTargetTexture(target);

        m_SubtractKernel.Dispatch(target.width, target.height);
    }

    public void Clear(RenderTexture target)
    {
        m_ClearKernel.SetTargetTexture(target);
        m_ClearKernel.Dispatch(target.width, target.height);
    }

    public void Copy(Texture source, RenderTexture target)
    {
        m_CopyKernel.SetSourceTexture(source);
        m_CopyKernel.SetTargetTexture(target);
        m_CopyKernel.Dispatch(target.width, target.height);
    }
}
