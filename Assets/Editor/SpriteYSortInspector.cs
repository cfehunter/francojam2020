﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(SpriteYSort)), CanEditMultipleObjects]
public class SpriteYSortInspector : Editor
{
    SpriteYSort m_Target;

    void OnEnable()
    {
        m_Target = (SpriteYSort)target;
    }

    public override void OnInspectorGUI()
    {
        DrawDefaultInspector();

        if (targets.Length == 1)
        {
            if (GUILayout.Button("Log Info"))
            {
                Debug.Log($"{m_Target.gameObject.name} | Position: {m_Target.transform.position2D()} | Pivot Point: {GetPivot()} | Sort Level: {SpriteYSort.SortLevel(GetPivot().y)}");
            }
        }
    }

    private Vector2 GetPivot()
    {
        SpriteRenderer sprite = m_Target.GetComponent<SpriteRenderer>();
        Vector2 result = sprite.bounds.center;
        result.y = sprite.bounds.min.y + m_Target.m_PivotOffset;
        return result;
    }
}
