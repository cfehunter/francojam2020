﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

public class SplatUndo : ScriptableObject
{
    class Snapshot
    {
        public Texture2D  Texture;
        public int        ID;
        public GameObject Object;
    }

    const int MaxSnapshots = 50;
    [SerializeField] int m_ID;
    static List<Snapshot> m_Snapshots = new List<Snapshot>();
    public static GameObject LatestObject => m_Snapshots.Back()?.Object;
    static int s_NextID = 0;

    private void OnValidate()
    {
        SplatPainter painter = SplatPainter.Instance;
        if (painter)
        {
            Snapshot snapshot = m_Snapshots.Find(x => x.ID == m_ID);
            if (snapshot != null)
            {
                painter.RestoreSnapshot(snapshot.Texture);
            }
            //Unity doesn't differentiate between undo and redo...
            else if (m_ID > 0)
            {
                snapshot = m_Snapshots.Find(x => x.ID == m_ID - 1);
                if (snapshot != null)
                {
                    painter.RestoreSnapshot(snapshot.Texture);
                }
            }
        }
    }

    public static void ClearSnapshots()
    {
        m_Snapshots.Clear();
    }

    public void SaveSnapshot(RenderTexture target, GameObject target_object)
    {
        Undo.RecordObject(this, "Splat Paint");
        Texture2D snapshot_texture = new Texture2D(target.width, target.height, TextureFormat.ARGB32, false);
        Graphics.CopyTexture(target, snapshot_texture);

        m_ID = ++s_NextID;
        m_Snapshots.Add(new Snapshot { ID = m_ID, Texture = snapshot_texture, Object = target_object });

        if (m_Snapshots.Count > MaxSnapshots)
        {
            m_Snapshots.RemoveAt(0);
        }
    }

}
