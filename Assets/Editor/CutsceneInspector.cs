﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;
using UnityEditor;

[CustomEditor(typeof(CutsceneDefinition))]
public class CutsceneInspector : Editor
{
    CutsceneDefinition m_Cutscene;
    List<DialogLine> m_ExpandedLines = new List<DialogLine>();
    static readonly string ExportHeader = "Name\tName Colour\tContent\tContent Colour\tContent Alignment\tPortrait\tAudio";

    void OnEnable()
    {
        m_Cutscene = (CutsceneDefinition)target;
    }

    private void OnDisable()
    {
        m_ExpandedLines.Clear();
    }

    public override void OnInspectorGUI()
    {
        base.OnInspectorGUI();

        GUILayout.BeginHorizontal();
        if (GUILayout.Button("Copy"))
        {
            CopyToClipboard();
        }

        if (GUILayout.Button("Paste"))
        {
            PasteFromClipboard();
        }
        GUILayout.EndHorizontal();

        serializedObject.ApplyModifiedProperties();
    }

    private void PasteFromClipboard()
    {
        string content = GUIUtility.systemCopyBuffer;
        string[] lines = content.Split(new char[] { '\r', '\n' }, StringSplitOptions.RemoveEmptyEntries);

        List<DialogLine> parsed_lines = new List<DialogLine>(lines.Length);

        foreach (string line in lines)
        {
            if (line == ExportHeader)
                continue;

            string[] fields = line.Split('\t');
            if (fields.Length == 7)
            {
                DialogLine new_line = new DialogLine();

                new_line.NameText   = fields[0];
                new_line.ContentText = fields[2];

                //Conditional parses. Fail if anything fails
                if (!(TryParseColour(fields[1], out new_line.NameColour)
                && TryParseColour(fields[3], out new_line.ContentColour)
                && TryParseEnum(fields[4], out new_line.ContentAlignment)
                && TryLoadAsset(fields[5], out new_line.Portrait)
                && TryLoadAsset(fields[6], out new_line.Audio)))
                {
                    return;
                }

                parsed_lines.Add(new_line);
            }
        }

        if (parsed_lines.Count == 0)
        {
            EditorUtility.DisplayDialog("Invalid Paste", "No records found in clipboard data", "Okay");
        }
        else if (EditorUtility.DisplayDialog("Confirm Paste", $"Found {parsed_lines.Count} valid entries.\nReplacing {m_Cutscene.DialogLines.Count} existing entries.\nDo you want to continue?", "Yes", "No"))
        {
            var modify_lines = (List<DialogLine>)m_Cutscene.DialogLines;
            modify_lines.Clear();
            modify_lines.AddRange(parsed_lines);
            EditorUtility.SetDirty(m_Cutscene);
        }
    }

    private void CopyToClipboard()
    {
        StringBuilder sb = new StringBuilder();
        sb.AppendLine(ExportHeader);

        foreach (DialogLine line in m_Cutscene.DialogLines)
        {
            sb.AppendLine($"{line.NameText}\t#{ColorUtility.ToHtmlStringRGB(line.NameColour)}\t{line.ContentText}\t#{ColorUtility.ToHtmlStringRGB(line.ContentColour)}\t{line.ContentAlignment}\t{AssetDatabase.GetAssetPath(line.Portrait)}\t{AssetDatabase.GetAssetPath(line.Audio)}");
        }
        GUIUtility.systemCopyBuffer = sb.ToString();
    }

    private bool TryParseEnum<T>(string text, out T result) where T : struct
    {
        if (Enum.TryParse(text, out result))
        {
            return true;
        }
        else
        {
            string type_name = typeof(T).Name;
            EditorUtility.DisplayDialog($"Invalid {type_name}", $"Error: [{text}] is not a valid {type_name}\nExpected one of:\n{Enum.GetNames(typeof(T)).Aggregate((ag, s) => ag + "\n" + s)}", "Okay");
            return false;
        }
    }

    private bool TryParseColour(string text, out Color result)
    {
        if (ColorUtility.TryParseHtmlString(text, out result))
        {
            return true;
        }
        else
        {
            EditorUtility.DisplayDialog("Invalid Colour", $"Error: [{text}] is not a valid colour string", "Okay");
            return false;
        }
    }

    private bool TryLoadAsset<T>(string text, out T result) where T : UnityEngine.Object
    {
        result = null;

        if (text.Length == 0)
            return true;

        result = AssetDatabase.LoadAssetAtPath<T>(text);
        if (!result)
        {
            EditorUtility.DisplayDialog("Invalid Asset Path", $"Error: [{text}] is not a valid {typeof(T).Name}", "Okay");
            return false;
        }
        return true;
    }
}
