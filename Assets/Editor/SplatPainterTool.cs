﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using UnityEditor.EditorTools;

public class SplatPainterTool : EditorTool
{
    public override void OnToolGUI(EditorWindow window)
    {
        base.OnToolGUI(window);

        SplatPainter painter = SplatPainter.Instance;
        if (!painter || !painter.TargetBox || painter.BrushSize <= 0f)
            return;

        Event current_evt = Event.current;
        if (current_evt.type == EventType.MouseUp && current_evt.button == 0)
        {
            painter.EndDraw();
            return;
        }

        var (contact_point, hit_target) = CastBrush(painter.TargetBox, painter.BrushSize, painter.BrushFeather);
        if (!hit_target)
            return;

        HandleUtility.AddDefaultControl(GUIUtility.GetControlID(FocusType.Passive));
        switch (current_evt.type)
        {
            case EventType.MouseDown:
            case EventType.MouseDrag:
                if (current_evt.button == 0)
                {
                    painter.PaintPixels(contact_point.XY());
                    current_evt.Use();
                }
                break;
        }

    }

    private (Vector3, bool) CastBrush(BoxCollider2D target_box, float brush_size, float feather)
    {
        Ray ui_ray = HandleUtility.GUIPointToWorldRay(Event.current.mousePosition);
        RaycastHit2D hit_info = Physics2D.GetRayIntersection(ui_ray, float.PositiveInfinity, 1 << target_box.gameObject.layer);
        if (hit_info.collider == target_box)
        {
            Vector3 box_pos_3d = target_box.transform.position;
            Vector3 contact_point = new Vector3(hit_info.point.x, hit_info.point.y, box_pos_3d.z);
            Handles.color = Color.red;
            Handles.DrawWireDisc(contact_point, Vector3.forward, 0.5f * brush_size);

            if (feather > 0f)
            {
                Handles.color = Color.yellow;
                Handles.DrawWireDisc(contact_point, Vector3.forward, 0.5f * brush_size * (1f - feather));
            }

            SceneView.RepaintAll();
            return (contact_point, true);
        }
        return (Vector3.zero, false);
    }
}
