﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

public class TestBlit : EditorWindow
{
    private ComputeShader m_PaintShader;
    private RenderTexture m_Texture;
    private int           m_PaintKernelIndex = 0;
    private int           m_PaintThreadsX;
    private int           m_PaintThreadsY;
    private Vector2       m_EllipsePos;
    private Vector2       m_EllipseSize;
    private Texture2D     m_BG;

    private void OnEnable()
    {
        if (!m_BG)
        {
            m_BG = new Texture2D(512, 512);
            m_BG.Fill(new Color(0.2f, 0.4f, 0.6f, 1.0f));
            m_BG.Apply();
        }

        m_EllipsePos = new Vector2(256f, 256f);
        m_EllipseSize = new Vector2(50f, 100f);

        if (!m_Texture)
        {
            m_Texture = new RenderTexture(512, 512, 0, RenderTextureFormat.ARGB32);
            m_Texture.enableRandomWrite = true;
            m_Texture.Create();
        }

        if (!m_PaintShader)
        {
            m_PaintShader = AssetDatabase.LoadAssetAtPath<ComputeShader>("Assets/Shader/Paint.compute");
            Debug.Assert(m_PaintShader);
            m_PaintKernelIndex = m_PaintShader.FindKernel("CSMain");

            //Query rquires uints, dispatch requires ints. Weird interface quirk...
            uint upaint_threads_x;
            uint upaint_threads_y;
            m_PaintShader.GetKernelThreadGroupSizes(m_PaintKernelIndex, out upaint_threads_x, out upaint_threads_y, out uint _);

            m_PaintThreadsX = (int)upaint_threads_x;
            m_PaintThreadsY = (int)upaint_threads_y;

            m_PaintShader.SetTexture(m_PaintKernelIndex, "Result", m_Texture);

            //Figure out the min/max bounds of the ellipse

            //Vector2 offset = new Vector2(m_EllipseSpec.x - m_EllipseSpec.z * 0.5f, m_EllipseSpec.y - m_EllipseSpec.w * 0.5f);
            //Vector4 transformed_ellipse = m_EllipseSpec;
            //transformed_ellipse.x -= offset.x;
            //transformed_ellipse.y -= offset.y;
            //m_PaintShader.SetVector("Offset", offset);
            //m_PaintShader.SetVector("EllipseSpec", transformed_ellipse);

            //Whole Texture
            //m_PaintShader.SetVector("Offset", Vector2.zero);
            //m_PaintShader.SetVector("EllipseSpec", new Vector4(m_EllipsePos.x, m_EllipsePos.y, m_EllipseSize.x, m_EllipseSize.y));
            //int runs_x = (m_Texture.width + m_PaintThreadsX - 1) / m_PaintThreadsX;
            //int runs_y = (m_Texture.height + m_PaintThreadsY - 1) / m_PaintThreadsY;
            //m_PaintShader.Dispatch(m_PaintKernelIndex, runs_x, runs_y, 1);

            //Dirty Region
            Vector2 offset = m_EllipsePos - m_EllipseSize;
            m_PaintShader.SetVector("Offset", offset);
            Vector2 transformed_ellipse_pos = m_EllipsePos - offset;
            m_PaintShader.SetVector("EllipseSpec", new Vector4(transformed_ellipse_pos.x, transformed_ellipse_pos.y, m_EllipseSize.x, m_EllipseSize.y));
            int runs_x = ((int)m_EllipseSize.x * 2 + m_PaintThreadsX - 1) / m_PaintThreadsX;
            int runs_y = ((int)m_EllipseSize.y * 2 + m_PaintThreadsY - 1) / m_PaintThreadsY;
            m_PaintShader.Dispatch(m_PaintKernelIndex, runs_x, runs_y, 1);
        }
    }

    private void OnGUI()
    {
        GUI.DrawTexture(new Rect(0f, 0f, m_BG.width, m_BG.height), m_BG);
        GUI.DrawTexture(new Rect(0f, 0f, m_Texture.width, m_Texture.height), m_Texture);
    }

    [MenuItem(itemName: "Franco Jam/Blit Test")]
    public static void RunTestBlit()
    {
        GetWindow<TestBlit>("Blit Test");
    }
}
