﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using UnityEditor;
using UnityEditor.EditorTools;


public class SplatPainter : EditorWindow
{
    const string EditModeKeyword = "EDITMODE_ON";
    const string SplatShaderName = "Shader Graphs/SplatMap";

    enum PaintMode
    {
        Add,
        Subtract,
        Clear
    }

    enum PaintChannel
    {
        Red,
        Green,
        Blue,
        Alpha
    }

    private static readonly int PaintDeltaMapID = Shader.PropertyToID("_PaintDelta");
    private static readonly int PaintDeltaScaleID = Shader.PropertyToID("_PaintScale");
    private static readonly int SplatMapID = Shader.PropertyToID("SplatMap");

    EllipsePainter        m_EllipsePainter;
    bool                  m_BrushExpanded = true;
    bool                  m_TexturesExpanded = false;
    bool                  m_SplatmapExpanded = false;
    float                 m_BrushAlpha = 1.0f;
    int                   m_MapWidth = 512;
    int                   m_MapHeight = 512;
    PaintChannel          m_PaintChannel = PaintChannel.Red;
    PaintMode             m_PaintMode = PaintMode.Add;
    Material              m_TargetMaterial;
    Texture2D             m_TargetTexture;
    RenderTexture         m_TargetRenderTexture;
    RenderTexture         m_DeltaTexture;
    string                m_TexturePath = string.Empty;
    SplatUndo             m_undo;
    bool                  m_TargetIsDirty = false;
    public BoxCollider2D  TargetBox { get; private set; }
    public float          BrushSize { get; private set; } = 1.0f;
    public float          BrushFeather { get; private set; } =  0.0f;


    [MenuItem(itemName: "Franco Jam/Splat Map Editor")]
    public static void RunSplatPainter()
    {
        GetWindow<SplatPainter>("Splat Map Editor");
    }

    public static SplatPainter Instance { get; private set; }

    private void OnEnable()
    {
        Instance = this;
        m_undo = CreateInstance<SplatUndo>();
        if (m_EllipsePainter == null)
        {
            m_EllipsePainter = new EllipsePainter();
        }
        UpdateTargets();
    }

    private void OnDisable()
    {
        PromptSaveIfDirty();

        RestoreMaterial();

        Instance = null;
        if (m_DeltaTexture)
        {
            m_DeltaTexture.Release();
            m_DeltaTexture = null;
        }
        if (m_TargetRenderTexture)
        {
            m_TargetRenderTexture.Release();
            m_TargetRenderTexture = null;
        }
    }

    private void OnSelectionChange()
    {
        PromptSaveIfDirty();
        UpdateTargets();
        Repaint();
    }

    private void OnGUI()
    {
        EditorGUILayout.BeginVertical();
        if (m_TargetMaterial)
            DrawEditorGUI();
        else
            EditorGUILayout.LabelField("No Splatmap Selected. Make sure you've assigned the correct material");

        EditorGUILayout.EndVertical();
    }

    public void EndDraw()
    {
        SaveInitialSnapshot();
        switch (m_PaintMode)
        {
            case PaintMode.Add:
                m_EllipsePainter.AddTexture(m_DeltaTexture, m_TargetRenderTexture);
                break;
            case PaintMode.Subtract:
            case PaintMode.Clear:
                m_EllipsePainter.SubtractTexture(m_DeltaTexture, m_TargetRenderTexture);
                break;
        }
        SaveSnapshot();
        m_EllipsePainter.Clear(m_DeltaTexture);

        m_TargetIsDirty = true;
    }

    public void PaintPixels(Vector2 contact_point)
    {
        Vector2 world_bounds = TargetBox.bounds.max - TargetBox.bounds.min;
        Vector2Int texture_bounds = new Vector2Int(m_TargetTexture.width, m_TargetTexture.height);
        Vector2 pixels_per_world_unit = new Vector2(texture_bounds.x / world_bounds.x, texture_bounds.y / world_bounds.y);

        Vector2 top_left = new Vector2(TargetBox.bounds.min.x, TargetBox.bounds.max.y);
        Vector2 local_contact = contact_point - top_left;
        local_contact.y = -local_contact.y;

        Vector2 contact_pixel = local_contact * pixels_per_world_unit;
        //X axis seems flipped on the Unity cube
        contact_pixel.x = texture_bounds.x - contact_pixel.x;

        Vector2 brush_pixel_radius = (pixels_per_world_unit * BrushSize * 0.5f);
        m_EllipsePainter.DrawEllipse(m_DeltaTexture, contact_pixel, brush_pixel_radius.x, brush_pixel_radius.y, GetBrushColour(), BrushFeather);
    }

    public void RestoreSnapshot(Texture2D undo_snapshot)
    {
        if (m_TargetTexture.width != undo_snapshot.width || m_TargetTexture.height != undo_snapshot.height)
        {
            m_TargetTexture.Resize(undo_snapshot.width, undo_snapshot.width);
            m_TargetTexture.Apply();
        }
        Graphics.CopyTexture(undo_snapshot, m_TargetTexture);

        m_DeltaTexture = UpdateRenderTexture(m_DeltaTexture);
        m_TargetRenderTexture = UpdateRenderTexture(m_TargetRenderTexture);
        EnableMaterialEditMode();

        m_TargetIsDirty = true;
    }

    private void SaveSnapshot()
    {
        if (m_TargetRenderTexture)
            m_undo.SaveSnapshot(m_TargetRenderTexture, TargetBox.gameObject);
    }

    private void SaveInitialSnapshot()
    {
        if (SplatUndo.LatestObject != TargetBox.gameObject)
            SaveSnapshot();
    }

    private void DrawEditorGUI()
    {
        if (m_BrushExpanded = EditorGUILayout.BeginFoldoutHeaderGroup(m_BrushExpanded, "Brush"))
        {
            if (m_TargetTexture)
            {
                DrawToolToggle();
                PaintMode new_mode = (PaintMode)EditorGUILayout.EnumPopup("Mode", m_PaintMode);
                if (new_mode != m_PaintMode)
                {
                    m_PaintMode = new_mode;
                    SetPaintScale();
                }

                BrushSize = EditorGUILayout.Slider("Brush Size", BrushSize, 0f, 20f);
                m_BrushAlpha = EditorGUILayout.Slider("Brush Alpha", m_BrushAlpha, 0f, 1f);
                BrushFeather = EditorGUILayout.Slider("Brush Feather", BrushFeather, 0f, 1f);

                EditorGUI.BeginDisabledGroup(new_mode == PaintMode.Clear);
                m_PaintChannel = (PaintChannel)EditorGUILayout.EnumPopup("Channel", m_PaintChannel);
                EditorGUI.EndDisabledGroup();
            }
            else
            {
                EditorGUILayout.LabelField("Please Create a Splatmap (below)");
            }
        }
        EditorGUILayout.EndFoldoutHeaderGroup();

        if (m_TexturesExpanded = EditorGUILayout.BeginFoldoutHeaderGroup(m_TexturesExpanded, "Textures"))
        {
            DrawVector2Field("Tiling", "Tiling");
            DrawTextureField("RedTexture", "Red");
            DrawTextureField("GreenTexture", "Green");
            DrawTextureField("BlueTexture", "Blue");
            DrawTextureField("AlphaTexture", "Alpha");
        }
        EditorGUILayout.EndFoldoutHeaderGroup();

        if (m_SplatmapExpanded = EditorGUILayout.BeginFoldoutHeaderGroup(m_SplatmapExpanded, "Splat Map"))
        {
            DrawTextureField("SplatMap", "Splat Map");

            EditorGUILayout.BeginHorizontal();
            m_MapWidth = EditorGUILayout.IntField("Width", m_MapWidth);
            m_MapHeight = EditorGUILayout.IntField("Height", m_MapHeight);
            EditorGUILayout.EndHorizontal();

            bool non_po2_sizes = !Mathf.IsPowerOfTwo(m_MapHeight) || !Mathf.IsPowerOfTwo(m_MapWidth);
            if (non_po2_sizes)
            {
                EditorGUILayout.LabelField("Please make your sizes power of two...");
            }
            EditorGUI.BeginDisabledGroup(non_po2_sizes);
            if (m_TargetTexture)
            {
                if (GUILayout.Button("Resize Map"))
                {
                    m_TargetTexture.Resize(m_MapWidth, m_MapHeight);
                }
            }
            else
            {
                if (GUILayout.Button("Create"))
                {
                    Texture2D new_texture = new Texture2D(m_MapWidth, m_MapHeight, TextureFormat.RGBA32, false);

                    //Unity initialises the texture to a seemingly random colour (0.804, 0.804, 0.804, 0.804)
                    //It also doesn't provide a flood fill method, so I have to allocate a full set of pixels here...
                    Color[] clear_pixels = new Color[m_MapWidth * m_MapHeight];
                    clear_pixels.Fill(Color.clear);
                    new_texture.SetPixels(clear_pixels);
                    new_texture.Apply();

                    m_TexturePath = DoSaveAs();
                    SaveTexture(m_TexturePath);

                    if (m_TexturePath.Length > 0)
                    {
                        AssetDatabase.Refresh();
                        m_TargetTexture = (Texture2D)AssetDatabase.LoadAssetAtPath(m_TexturePath, typeof(Texture2D));
                        if (m_TargetTexture)
                        {
                            m_TargetMaterial.SetTexture(SplatMapID, m_TargetTexture);
                        }
                        else
                        {
                            m_TexturePath = string.Empty;
                        }
                        GUIUtility.ExitGUI();
                    }
                }
            }
        }
        EditorGUILayout.EndFoldoutHeaderGroup();


        if (m_TargetTexture)
        {
            EditorGUILayout.LabelField("/!\\ Save /!\\");

            EditorGUILayout.LabelField($"Path: {m_TexturePath}");

            EditorGUI.BeginDisabledGroup(m_TexturePath.Length == 0);
            if (GUILayout.Button("Save"))
            {
                string save_path = Path.Combine(Application.dataPath, m_TexturePath.Substring("Assets/".Length)); //Franco Jam hack...
                SaveTexture(save_path);
            }
            EditorGUI.EndDisabledGroup();

            if (GUILayout.Button("Save As"))
            {
                string result_path = DoSaveAs();
                if (result_path.Length > 0)
                {
                    SaveTexture(result_path);
                }
            }
        }
    }

    private void DrawToolToggle()
    {
        bool tool_was_active = EditorTools.activeToolType == typeof(SplatPainterTool);
        bool tool_toggle = GUILayout.Toggle(tool_was_active, "Paint", "Button");

        if (tool_toggle != tool_was_active)
        {
            if (tool_toggle)
                EditorTools.SetActiveTool<SplatPainterTool>();
            else
                EditorTools.RestorePreviousTool();
        }

    }

    private string DoSaveAs()
    {
        return EditorUtility.SaveFilePanelInProject("Save Splatmap", "New SplatMap", "png", "Select a location to save the new splatmap");
    }

    private void DrawTextureField(string property, string label)
    {
        Texture existing = m_TargetMaterial.GetTexture(property);
        Texture new_texture = (Texture)EditorGUILayout.ObjectField(label, existing, typeof(Texture), false);
        if (new_texture != existing)
        {
            m_TargetMaterial.SetTexture(property, new_texture);
            EditorUtility.SetDirty(m_TargetMaterial);
        }
    }

    private void DrawVector2Field(string property, string label)
    {
        Vector2 existing = m_TargetMaterial.GetVector(property);
        Vector2 new_vec = EditorGUILayout.Vector2Field(label, existing);
        if (!existing.Approximately(new_vec))
        {
            m_TargetMaterial.SetVector(property, new_vec);
        }
    }

    private void UpdateTargets()
    {
        RestoreMaterial();

        m_TargetMaterial = null;
        m_TargetTexture = null;
        TargetBox = null;

        GameObject active_object = Selection.activeGameObject;
        if (!active_object)
            return;

        BoxCollider2D box = active_object.GetComponent<BoxCollider2D>();
        if (!box)
            return;

        MeshRenderer renderer = Selection.activeGameObject?.GetComponent<MeshRenderer>();
        if (!renderer)
            return;

        Material material = renderer.sharedMaterial;
        if (!material)
            return;

        if (material.shader.name == SplatShaderName)
        {
            m_TargetMaterial = material;
            m_TargetTexture = (Texture2D)material.GetTexture(SplatMapID);
            m_TexturePath = AssetDatabase.GetAssetPath(m_TargetTexture);
            TargetBox = box;
            m_DeltaTexture = UpdateRenderTexture(m_DeltaTexture);
            m_TargetRenderTexture = UpdateRenderTexture(m_TargetRenderTexture);
            EnableMaterialEditMode();
        }
        else
        {
            m_DeltaTexture.Release();
            m_TargetRenderTexture.Release();
            m_DeltaTexture = m_TargetRenderTexture = null;
        }

        m_TargetIsDirty = false;
    }

    private RenderTexture UpdateRenderTexture(RenderTexture render_texture)
    {
        if (m_TargetTexture)
        {
            if (!render_texture || render_texture.width != m_TargetTexture.width || render_texture.height != m_TargetTexture.height)
            {
                if (render_texture)
                {
                    render_texture.Release();
                    render_texture = null;
                }

                render_texture = new RenderTexture(m_TargetTexture.width, m_TargetTexture.height, 1, RenderTextureFormat.ARGB32);
                render_texture.enableRandomWrite = true;
                render_texture.Create();
            }
        }
        else if(render_texture)
        {
            render_texture.Release();
            render_texture = null;
        }

        return render_texture;
    }

    private void EnableMaterialEditMode()
    {
        if (m_TargetMaterial)
        {
            List<string> keywords = new List<string>(m_TargetMaterial.shaderKeywords);
            keywords.Add(EditModeKeyword);
            m_TargetMaterial.shaderKeywords = keywords.ToArray();
            m_TargetMaterial.SetTexture(PaintDeltaMapID, m_DeltaTexture);

            m_EllipsePainter.Copy(m_TargetTexture, m_TargetRenderTexture);
            m_TargetMaterial.SetTexture(SplatMapID, m_TargetRenderTexture);

            SetPaintScale();
        }
    }

    private void RestoreMaterial()
    {
        if (m_TargetMaterial && m_TargetRenderTexture)
        {
            List<string> keywords = new List<string>(m_TargetMaterial.shaderKeywords);
            keywords.Remove(EditModeKeyword);

            //Copy the render texture out to the final texture
            Graphics.CopyTexture(m_TargetRenderTexture, m_TargetTexture);

            m_TargetMaterial.shaderKeywords = keywords.ToArray();
            m_TargetMaterial.SetTexture(PaintDeltaMapID, null);
            m_TargetMaterial.SetTexture(SplatMapID, m_TargetTexture);
        }
    }

    private Color GetBrushColour()
    {
        if (m_PaintMode == PaintMode.Clear)
        {
            return Color.white;
        }

        switch (m_PaintChannel)
        {
            case PaintChannel.Red:
                return new Color(m_BrushAlpha, 0f, 0f, 0f);
            case PaintChannel.Green:
                return new Color(0f, m_BrushAlpha, 0f, 0f);
            case PaintChannel.Blue:
                return new Color(0f, 0f, m_BrushAlpha, 0f);
            case PaintChannel.Alpha:
            default:
                return new Color(0f, 0f, 0f, m_BrushAlpha);
        }
    }

    private void SetPaintScale()
    {
        m_TargetMaterial.SetFloat(PaintDeltaScaleID, m_PaintMode == PaintMode.Add ? 1f : -1f);
    }

    private void SaveTexture(string path)
    {
        //Copy the render texture out to the final texture
        RenderTexture prev_rt = RenderTexture.active;
        RenderTexture.active = m_TargetRenderTexture;
        m_TargetTexture.ReadPixels(new Rect(0, 0, m_TargetTexture.width, m_TargetTexture.height), 0, 0);
        m_TargetTexture.Apply();

        //Backup the file
        string backup_path = string.Empty;
        if (File.Exists(path))
        {
            backup_path = $"{path}.old";
            File.Copy(path, backup_path, true);
        }

        try
        {
            File.WriteAllBytes(path, m_TargetTexture.EncodeToPNG());
        }
        catch (System.Exception ex)
        {
            //Restore the backup
            if (File.Exists(backup_path))
            {
                File.Copy(backup_path, path, true);
            }
            Debug.LogError(ex);
        }

        //Delete the backup
        if (File.Exists(backup_path))
        {
            File.Delete(backup_path);
        }

        RenderTexture.active = prev_rt;
        m_TargetIsDirty = false;
    }

    private void PromptSaveIfDirty()
    {
        if (m_TargetIsDirty && m_TargetTexture && m_TexturePath.Length > 0
            && EditorUtility.DisplayDialog("Save Changes?", $"You have unsaved changes in splat map would you like to save them?\n{m_TargetTexture.name}", "Save", "Discard"))
        {
            SaveTexture(m_TexturePath);
        }

        m_TargetIsDirty = false;
    }
}
